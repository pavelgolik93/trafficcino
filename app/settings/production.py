from .base import *

import sentry_sdk
from sentry_sdk.integrations.django import DjangoIntegration


# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False

HOST_NAME = "app.trafficcino.com"
LANDING_HOST_NAME = "shop.trafficcino.com"

ALLOWED_HOSTS = [
    HOST_NAME,
    LANDING_HOST_NAME,
]
CSRF_TRUSTED_ORIGINS = [
    "https://app.trafficcino.com",
]


# Sentry
sentry_sdk.init(
    dsn=os.environ["SENTRY_CONNECTION_STRING"],
    integrations=[DjangoIntegration()],
    traces_sample_rate=1.0,
    send_default_pii=True,
)
