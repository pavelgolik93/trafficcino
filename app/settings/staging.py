from django.urls import path, include

from .base import *


# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

HOST_NAME = "trafficcino-front-master.aitarget.com"
LANDING_HOST_NAME = "trafficcino-landing-master.aitarget.com"

ALLOWED_HOSTS = ["*"]
CSRF_TRUSTED_ORIGINS = [
    "http://localhost:1111",
    "https://*.aitarget.com",
]


# Application definition

INSTALLED_APPS.extend(
    [
        "corsheaders",
        "debug_toolbar",
    ]
)

MIDDLEWARE.insert(0, "corsheaders.middleware.CorsMiddleware")
MIDDLEWARE.insert(1, "debug_toolbar.middleware.DebugToolbarMiddleware")


# CORS

CORS_ALLOW_ALL_ORIGINS = True


# Rest Framework

REST_FRAMEWORK["DEFAULT_RENDERER_CLASSES"].append(
    "rest_framework.renderers.BrowsableAPIRenderer"
)


# Debug Toolbar

DEBUG_TOOLBAR_CONFIG = {
    "SHOW_TOOLBAR_CALLBACK": lambda request: True,
}
