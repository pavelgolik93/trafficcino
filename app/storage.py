import mimetypes

import boto3
from django.conf import settings
from django.core.files.storage import Storage
from django.utils.deconstruct import deconstructible
from django.utils.functional import cached_property


@deconstructible
class AWSStorage(Storage):
    def __init__(self):
        self.region_name = settings.AWS_REGION_NAME
        self.endpoint_url = settings.AWS_ENDPOINT_URL
        self.bucket_name = settings.AWS_BUCKET_NAME
        self.aws_access_key_id = settings.AWS_ACCESS_KEY_ID
        self.aws_secret_access_key = settings.AWS_SECRET_ACCESS_KEY

    @property
    def aws_client(self):
        session = boto3.Session()
        return session.client(
            "s3",
            region_name=self.region_name,
            endpoint_url=self.endpoint_url,
            aws_access_key_id=self.aws_access_key_id,
            aws_secret_access_key=self.aws_secret_access_key,
        )

    @cached_property
    def base_url(self):
        if hasattr(settings, "MEDIA_URL"):
            return (
                settings.MEDIA_URL[:-1]
                if settings.MEDIA_URL.endswith("/")
                else settings.MEDIA_URL
            )
        return None

    def url(self, name):
        if self.base_url is None:
            raise ValueError("This file is not accessible via a URL.")
        return self.base_url + name

    def mime_type(self, name):
        file_type, _ = mimetypes.guess_type(name)
        return file_type

    def save(self, name, content, max_length=None):
        self.aws_client.upload_fileobj(
            content,
            self.bucket_name,
            name,
            ExtraArgs={"ContentType": self.mime_type(name)},
        )
        return "/{}/{}".format(self.bucket_name, name)

    # def delete(self, name):
    #     if not name:
    #         raise ValueError("The name must be given to delete().")
    #     name = self.path(name)
    #     # If the file or directory exists, delete it from the filesystem.
    #     try:
    #         if os.path.isdir(name):
    #             os.rmdir(name)
    #         else:
    #             os.remove(name)
    #     except FileNotFoundError:
    #         # FileNotFoundError is raised if the file or directory was removed
    #         # concurrently.
    #         pass
