import os
from io import BytesIO
from PIL import Image
from django.core import checks
from django.core.files.base import ContentFile
from django.db.models import ImageField
from django.db.models.fields.files import ImageFieldFile


class CompressedImageFieldFile(ImageFieldFile):
    def _proportional_image_resizing(self, image: Image.Image):
        ratio = float(self.field.max_width / float(image.width))
        height = int(float(image.height) * ratio)
        return image.resize((self.field.max_width, height), Image.ANTIALIAS)

    def _build_content(self, image: Image.Image, name: str):
        buffer = BytesIO()
        extension = os.path.splitext(name)[1][1:]
        format_ = "JPEG" if extension.lower() == "jpg" else extension.upper()
        image.save(buffer, format=format_, quality=95)
        return ContentFile(buffer.getvalue(), name)

    def save(self, name, content, save=True):
        content = self.compress_content(name, content)
        super(CompressedImageFieldFile, self).save(name, content, save)

    def compress_content(self, name, content):
        if content.image.width > self.field.max_width:
            image = Image.open(content)
            image = self._proportional_image_resizing(image)
            return self._build_content(image, name)
        return content


class CompressedImageField(ImageField):
    attr_class = CompressedImageFieldFile

    def __init__(self, max_width=None, **kwargs):
        self.max_width = max_width
        super(CompressedImageField, self).__init__(**kwargs)

    def check(self, **kwargs):
        return [
            *super().check(**kwargs),
            *self._check_max_width(),
        ]

    def _check_max_width(self):
        if self.max_width is None:
            return [
                checks.Error(
                    "'max_width' mus be set for a %s." % self.__class__.__name__,
                    obj=self,
                    id="fields.E210",
                )
            ]
        return []
