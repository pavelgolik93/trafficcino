from django.conf import settings
from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path("admin/", admin.site.urls),
    path(
        "api/private/analytics/",
        include("analytics.api.private_urls", namespace="private-analytics"),
    ),
    path(
        "api/public/analytics/",
        include("analytics.api.public_urls", namespace="public-analytics"),
    ),
    path(
        "api/private/landing/",
        include("landing.api.private_urls", namespace="private-landing"),
    ),
    path(
        "api/public/landing/",
        include("landing.api.public_urls", namespace="public-landing"),
    ),
    # path(
    #     "api/private/stripe/",
    #     include("stripe_charge.api.private_urls", namespace="private-stripe-charge"),
    # ),
    # path(
    #     "api/public/stripe/",
    #     include("stripe_charge.api.public_urls", namespace="public-stripe-charge"),
    # ),
    path(
        "api/private/user/",
        include("user.api.private_urls", namespace="private-user"),
    ),
    path(
        "api/public/user/",
        include("user.api.public_urls", namespace="public-user"),
    ),
]

if settings.DEBUG is True:
    urlpatterns.append(path("__debug__/", include("debug_toolbar.urls")))
