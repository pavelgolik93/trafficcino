from rest_framework.exceptions import NotFound


class RetrieveAPIMixin:
    selector_function = None

    def get_object(self, *args, **kwargs):
        assert self.selector_function is not None, (
            "'%s' should either include a `selector_function` attribute."
            % self.__class__.__name__
        )

        assert callable(self.selector_function) is True, (
            "'%s' `selector_function` must be callable." % self.__class__.__name__
        )

        obj = self.selector_function(**kwargs)
        if obj is None:
            raise NotFound
        return obj
