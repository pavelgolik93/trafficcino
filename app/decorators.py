import functools

import stripe
from django.conf import settings


def stripe_init_session_decorator(func):
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        stripe.api_key = settings.STRIPE_API_KEY
        result = func(*args, **kwargs)
        stripe.api_key = None
        return result

    return wrapper
