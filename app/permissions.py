import stripe
from django.conf import settings
from rest_framework.permissions import BasePermission
from stripe.error import SignatureVerificationError


class AllowOnlyStripeWebhooks(BasePermission):
    def has_permission(self, request, view):
        try:
            stripe.Webhook.construct_event(
                request.body,
                request.headers["STRIPE_SIGNATURE"],
                settings.STRIPE_WEBHOOK_SECRET,
            )
        except (ValueError, SignatureVerificationError):
            return False
        return True
