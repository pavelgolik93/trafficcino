#!/bin/bash
python manage.py loaddata \
  /app/user/fixtures/user.json \
  /app/landing/fixtures/marketplace.json \
  /app/landing/fixtures/ad_network.json \
  /app/landing/fixtures/landing_pool.json \
  /app/landing/fixtures/landing_pool_settings.json \
  /app/landing/fixtures/product.json \
  /app/landing/fixtures/product_source.json \
  /app/landing/fixtures/landing.json \
  /app/landing/fixtures/image.json \
  /app/analytics/fixtures/collected_data.json
