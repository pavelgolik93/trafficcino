bind = "0.0.0.0:80"
workers = 4
threads = 4
forwarded_allow_ips = "*"
disable_redirect_access_to_syslog = True
timeout = 60
