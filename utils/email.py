from django.conf import settings
from django.core.mail import EmailMultiAlternatives


def send_letter(
    subject: str,
    to_email: list | str,
    email_plaintext_message: str,
    email_html_message: str | None = None,
) -> None:
    if isinstance(to_email, str):
        to_email = [to_email]

    msg = EmailMultiAlternatives(
        subject,
        email_plaintext_message,
        from_email=settings.DEFAULT_FROM_EMAIL,
        to=to_email,
    )

    if email_html_message is not None:
        msg.attach_alternative(email_html_message, "text/html")
    msg.send()
