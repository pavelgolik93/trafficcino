from typing import Union
from uuid import UUID

from django.db.models import QuerySet, Prefetch, Q

import utils
from .models import (
    LandingPool,
    LandingPoolSettings,
    Product,
    ProductSource,
    Image,
    Landing,
    Marketplace,
    AdNetwork,
)


def landing_pools_by_user(user_id: int) -> QuerySet[LandingPool]:
    return LandingPool.objects.filter(user_id=user_id).order_by("-created_at").all()


def finished_landing_pools_by_user(user_id: int) -> QuerySet[LandingPool]:
    return (
        LandingPool.objects.filter(user_id=user_id, step=LandingPool.Steps.COMPLETE)
        .prefetch_related(
            Prefetch(
                "landings",
                queryset=Landing.objects.prefetch_related(
                    Prefetch(
                        "landing_pool__products__sources",
                        queryset=ProductSource.objects.select_related("marketplace"),
                    )
                ),
            )
        )
        .select_related()
        .order_by("-created_at")
        .all()
    )


def unfinished_landing_pools_by_user(user_id: int) -> QuerySet[LandingPool]:
    return landing_pools_by_user(user_id).exclude(step=LandingPool.Steps.COMPLETE)


def landing_pool_by_user(
    user_id: int, landing_pool_id: int
) -> Union[LandingPool, None]:
    query = (
        LandingPool.objects.prefetch_related(
            Prefetch("products", queryset=Product.objects.order_by("id"))
        )
        .prefetch_related(
            Prefetch(
                "products__sources",
                queryset=ProductSource.objects.order_by("id"),
            )
        )
        .prefetch_related(
            Prefetch(
                "products__sources__product_source_images",
                queryset=Image.objects.order_by("id"),
            )
        )
    )
    return utils.get_object(query, id=landing_pool_id, user_id=user_id)


def landing_pool_settings_by_user(
    user_id: int, landing_pool_id: int
) -> Union[LandingPoolSettings, None]:
    return utils.get_object(
        LandingPoolSettings,
        landing_pool_id=landing_pool_id,
        landing_pool__user_id=user_id,
    )


def landing_pool_products_by_user(
    user_id: int, landing_pool_id: int
) -> QuerySet[LandingPool]:
    return (
        Product.objects.filter(
            landing_pool__user_id=user_id,
            landing_pool_id=landing_pool_id,
        )
        .prefetch_related(
            Prefetch("product_images", queryset=Image.objects.order_by("id"))
        )
        .order_by("id")
        .all()
    )


def products_sources_by_user_landing_pool_and_product_id(
    user_id: int, landing_pool_id: int, product_id: int
) -> QuerySet[ProductSource]:
    return ProductSource.objects.filter(
        product__landing_pool__user_id=user_id,
        product__landing_pool_id=landing_pool_id,
        product_id=product_id,
    ).all()


def landing_get(landing_id: UUID) -> Union[Landing, None]:
    query = Landing.objects.select_related("landing_pool__settings").prefetch_related(
        Prefetch(
            "landing_pool__products",
            queryset=Product.objects.prefetch_related(
                Prefetch(
                    "product_images",
                    queryset=Image.objects.filter(is_selected=True).order_by(
                        "sort_index"
                    ),
                )
            )
            .prefetch_related(
                Prefetch(
                    "sources",
                    queryset=ProductSource.objects.select_related("marketplace")
                    .filter(status=ProductSource.Statuses.ACCEPT)
                    .order_by("id"),
                )
            )
            .order_by("sort_index"),
        )
    )
    return utils.get_object(query, id=landing_id, landing_pool__is_active=True)


def fro_private_preview_landing_get(landing_pool_id: int) -> Union[LandingPool, None]:
    query = LandingPool.objects.select_related("settings").prefetch_related(
        Prefetch(
            "products",
            queryset=Product.objects.prefetch_related(
                Prefetch(
                    "product_images",
                    queryset=Image.objects.filter(is_selected=True).order_by(
                        "sort_index"
                    ),
                )
            )
            .prefetch_related(
                Prefetch(
                    "sources",
                    queryset=ProductSource.objects.select_related("marketplace")
                    .filter(status=ProductSource.Statuses.ACCEPT)
                    .order_by("id"),
                )
            )
            .order_by("sort_index"),
        )
    )
    return utils.get_object(query, id=landing_pool_id, is_active=True)


def all_marketplaces_with_user(user_id: int) -> QuerySet[Marketplace]:
    return (
        Marketplace.objects.filter(
            Q(user_id__isnull=True) | Q(user_id=user_id), is_active=True
        )
        .order_by("id")
        .all()
    )


def ad_networks() -> QuerySet[AdNetwork]:
    return AdNetwork.objects.filter(is_active=True).order_by("id").all()
