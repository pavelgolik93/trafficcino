from django.contrib.contenttypes.fields import GenericRelation
from drf_writable_nested.mixins import (
    BaseNestedModelSerializer,
    NestedCreateMixin as BaseNestedCreateMixin,
    NestedUpdateMixin as BaseNestedUpdateMixin,
)
from rest_framework.exceptions import ValidationError


class CustomBaseNestedModelSerializer(BaseNestedModelSerializer):
    def _prefetch_related_instances(
        self, instance, field, field_name, field_source, related_data
    ):
        pk_list = self._extract_related_pks(field, related_data)
        relative_name = field_source or field_name

        instances = {
            str(related_instance.pk): related_instance
            for related_instance in getattr(instance, relative_name).filter(
                pk__in=pk_list
            )
        }
        return instances

    def update_or_create_reverse_relations(
        self, instance, reverse_relations, only_update=False
    ):
        # Update or create reverse relations:
        # many-to-one, many-to-many, reversed one-to-one
        for field_name, (
            related_field,
            field,
            field_source,
        ) in reverse_relations.items():

            # Skip processing for empty data or not-specified field.
            # The field can be defined in validated_data but isn't defined
            # in initial_data (for example, if multipart form data used)
            related_data = self.get_initial().get(field_name, None)
            if related_data is None:
                continue

            if related_field.one_to_one:
                # If an object already exists, fill in the pk so
                # we don't try to duplicate it
                pk_name = field.Meta.model._meta.pk.attname
                if pk_name not in related_data and "pk" in related_data:
                    pk_name = "pk"
                if pk_name not in related_data:
                    related_instance = getattr(instance, field_source, None)
                    if related_instance:
                        related_data[pk_name] = related_instance.pk

                # Expand to array of one item for one-to-one for uniformity
                related_data = [related_data]

            instances = self._prefetch_related_instances(
                instance, field, field_name, field_source, related_data
            )

            save_kwargs = self._get_save_kwargs(field_name)
            if isinstance(related_field, GenericRelation):
                save_kwargs.update(
                    self._get_generic_lookup(instance, related_field),
                )
            elif not related_field.many_to_many:
                save_kwargs[related_field.name] = instance

            new_related_instances = []
            errors = []
            for data in related_data:
                obj = instances.get(self._get_related_pk(data, field.Meta.model))
                if only_update is True and obj is None:
                    continue

                serializer = self._get_serializer_for_field(
                    field, instance=obj, data=data
                )
                try:
                    serializer.is_valid(raise_exception=True)
                    related_instance = serializer.save(**save_kwargs)
                    data["pk"] = related_instance.pk
                    data["pizda"] = True
                    new_related_instances.append(related_instance)
                    errors.append({})
                except ValidationError as exc:
                    errors.append(exc.detail)

            if any(errors):
                if related_field.one_to_one:
                    raise ValidationError({field_name: errors[0]})
                else:
                    raise ValidationError({field_name: errors})

            if related_field.many_to_many:
                # Add m2m instances to through model via add
                m2m_manager = getattr(instance, field_source)
                m2m_manager.add(*new_related_instances)

    def update_or_create_direct_relations(self, attrs, relations, only_update=False):
        for field_name, (field, field_source) in relations.items():
            obj = None
            data = self.get_initial()[field_name]
            model_class = field.Meta.model
            pk = self._get_related_pk(data, model_class)
            if pk:
                obj = model_class.objects.filter(
                    pk=pk,
                ).first()
            serializer = self._get_serializer_for_field(
                field,
                instance=obj,
                data=data,
            )

            if only_update is True and obj is None:
                continue

            try:
                serializer.is_valid(raise_exception=True)
                attrs[field_source] = serializer.save(
                    **self._get_save_kwargs(field_name)
                )
            except ValidationError as exc:
                raise ValidationError({field_name: exc.detail})


class NestedCreateMixin(CustomBaseNestedModelSerializer, BaseNestedCreateMixin):
    pass


class NestedUpdateMixin(CustomBaseNestedModelSerializer, BaseNestedUpdateMixin):
    pass


class NestedOnlyUpdateMixin(CustomBaseNestedModelSerializer, BaseNestedUpdateMixin):
    def update(self, instance, validated_data):
        relations, reverse_relations = self._extract_relations(validated_data)

        # Create or update direct relations (foreign key, one-to-one)
        self.update_or_create_direct_relations(
            validated_data, relations, only_update=True
        )

        # Update instance
        instance = super(NestedOnlyUpdateMixin, self).update(instance, validated_data)

        self.update_or_create_reverse_relations(
            instance, reverse_relations, only_update=True
        )
        instance.refresh_from_db()
        return instance
