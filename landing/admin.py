from django.conf import settings
from django.contrib import admin
from django.utils.safestring import mark_safe
from django.utils.translation import gettext_lazy as _

from landing.forms import AlwaysChangedBaseInlineFormSet, ProductImageInlineFormSet
from landing.models import (
    LandingPool,
    LandingPoolSettings,
    Landing,
    Product,
    ProductSource,
    Marketplace,
    AdNetwork,
)
from .models import Image


@admin.register(LandingPool)
class LandingPoolAdmin(admin.ModelAdmin):
    class LandingPoolSettingsInline(admin.StackedInline):
        model = LandingPoolSettings
        can_delete = False

    inlines = (LandingPoolSettingsInline,)
    list_display = ("__str__", "user", "step", "created_at")
    raw_id_fields = ("user",)
    readonly_fields = ("created_at",)


@admin.register(Landing)
class LandingPageAdmin(admin.ModelAdmin):
    list_display = (
        "id",
        "landing_pool",
        "ad_network",
        "landing_preview_link",
        "created_at",
    )
    list_filter = ("landing_pool", "ad_network")
    raw_id_fields = ("landing_pool",)
    fieldsets = (
        (
            None,
            {
                "fields": (
                    "landing_preview_link",
                    "is_active",
                    "landing_pool",
                    "ad_network",
                    "scripts",
                    "created_at",
                )
            },
        ),
    )
    add_fieldsets = (
        (
            None,
            {
                "fields": (
                    "is_active",
                    "landing_pool",
                    "ad_network",
                    "scripts",
                )
            },
        ),
    )
    readonly_fields = ("landing_preview_link", "created_at")

    def get_fieldsets(self, request, obj=None):
        if obj is not None:
            return self.fieldsets
        return self.add_fieldsets

    def landing_preview_link(self, instance):
        return mark_safe(
            "<a href='{0}' target='_blank'>show page</a>".format(
                instance.landing_preview_link
            )
        )

    landing_preview_link.short_description = _("landing page link")


@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    class ProductSourceInline(admin.StackedInline):
        model = ProductSource
        readonly_fields = ("created_at",)
        extra = 0
        show_change_link = True

    class ProductImageInline(admin.TabularInline):
        model = Image
        fk_name = "product"
        formset = ProductImageInlineFormSet
        fields = ("src", "is_selected", "sort_index", "created_at")
        readonly_fields = ("created_at",)
        extra = 0

    list_display = ("__str__", "landing_pool", "created_at")
    list_filter = ("landing_pool", "landing_pool__user")
    raw_id_fields = ("landing_pool",)
    readonly_fields = ("created_at",)

    inlines = (ProductSourceInline, ProductImageInline)


@admin.register(ProductSource)
class ProductSourceAdmin(admin.ModelAdmin):
    class ProductSourceImageInline(admin.TabularInline):
        model = Image
        fk_name = "product_source"
        formset = AlwaysChangedBaseInlineFormSet
        fields = ("src", "is_selected", "sort_index", "created_at")
        readonly_fields = ("created_at",)
        extra = 0

    list_display = ("sku", "marketplace", "product", "status", "created_at")
    list_filter = ("product", "product__landing_pool")
    raw_id_fields = ("product", "marketplace")
    readonly_fields = ("created_at",)

    inlines = (ProductSourceImageInline,)

    def save_formset(self, request, form, formset, change):
        if formset.model == Image:
            for child_form in formset.forms:
                child_form.instance.product = form.instance.product
                if "product" in form.changed_data:
                    child_form.save()
        formset.save()


@admin.register(AdNetwork)
class AdNetworkAdmin(admin.ModelAdmin):
    list_display = ("logo_and_name", "is_active")
    readonly_fields = ("created_at",)

    def logo_and_name(self, instance):
        return mark_safe(
            """
            <div style='display: flex;align-items: center;align-content: center;'>
                <img src='{}'/>
                <div style='margin-left: 2px;'>{}</div>
            </div>
            """.format(
                instance.logo.url, instance.name
            )
        )

    logo_and_name.short_description = "Name"


@admin.register(Marketplace)
class MarketplaceAdmin(admin.ModelAdmin):
    list_display = ("logo_and_name", "is_active", "user")
    raw_id_fields = ("user",)
    readonly_fields = ("created_at",)

    def logo_and_name(self, instance):
        url = (
            instance.logo.url
            if instance.logo
            else "{}ailab/public/trafficcino/landing/marketplaces/user_custom_marketplace.svg".format(
                settings.MEDIA_URL
            )
        )
        return mark_safe(
            """
            <div style='display: flex; align-items: center; align-content: center;'>
                <img src='{}'/>
                <div style='margin-left: 2px;'>{}</div>
            </div>
            """.format(
                url, instance.name
            )
        )

    logo_and_name.short_description = "Name"
