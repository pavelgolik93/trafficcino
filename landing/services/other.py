from django.db import transaction

import utils
from landing.models import ProductSource, Product, Image, LandingPoolSettings, Landing


def landing_pool_product_source_by_user_exist(
    user_id: int, landing_pool_id: int, product_source_id: int
) -> bool:
    return ProductSource.objects.filter(
        id=product_source_id,
        product__landing_pool__user_id=user_id,
        product__landing_pool_id=landing_pool_id,
    ).exists()


def landing_pool_product_by_user_exist(
    user_id: int, product_id: int, landing_pool_id: int
) -> bool:
    return Product.objects.filter(
        id=product_id, landing_pool__user_id=user_id, landing_pool_id=landing_pool_id
    ).exists()


@transaction.atomic()
def product_image_destroy(
    user_id: int, image_id: int, landing_pool_id: int, product_id: int
) -> bool:
    is_destroyed = False
    image = utils.get_object(
        Image,
        id=image_id,
        product_id=product_id,
        product__landing_pool_id=landing_pool_id,
        product__landing_pool__user_id=user_id,
    )

    if image is not None:
        image.delete()
        is_destroyed = True
    return is_destroyed


def product_source_clean_extra_data(product_source_id: int) -> None:
    ProductSource.objects.filter(pk=product_source_id).update(
        link="", title="", price=None
    )
    Image.objects.filter(product_source_id=product_source_id).delete()


@transaction.atomic()
def create_landings_by_selected_ad_networks(
    landing_pool_settings: LandingPoolSettings,
) -> None:
    Landing.objects.bulk_create(
        [
            Landing(
                landing_pool_id=landing_pool_settings.landing_pool_id,
                ad_network=ad_network,
            )
            for ad_network in landing_pool_settings.ad_networks.all()
        ]
    )
