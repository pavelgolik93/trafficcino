from django.conf import settings
from django.template.loader import render_to_string

import utils
from ..models import LandingPool, ProductSource


def notify_us_that_created_new_landing_pool(landing_pool: LandingPool) -> None:
    subject = "Trafficcino: Created new Landing Pool"
    context = {
        "user_email": landing_pool.user.email,
        "landing_pool_link": "https://{}/admin/landing/landingpool/{}/change/".format(
            settings.HOST_NAME, landing_pool.id
        ),
    }
    email_plaintext_message = render_to_string(
        "email/new_landing_pool_created.txt", context
    )

    utils.send_letter(
        subject,
        to_email=settings.DEFAULT_ADMIN_EMAILS,
        email_plaintext_message=email_plaintext_message,
    )


def notify_us_that_reanalyze_required(landing_pool: LandingPool) -> None:
    subject = "Trafficcino: Required reanalyze"

    product_sources = ProductSource.objects.filter(
        product__landing_pool_id=landing_pool.id, status=ProductSource.Statuses.ANALYSIS
    ).all()

    context = {
        "user_email": landing_pool.user.email,
        "product_source_links": [
            "https://{}/admin/landing/productsource/{}/change/".format(
                settings.HOST_NAME, p.id
            )
            for p in product_sources
        ],
    }
    email_plaintext_message = render_to_string(
        "email/landing_pool_reanalyze_required.txt", context
    )

    utils.send_letter(
        subject,
        to_email=settings.DEFAULT_ADMIN_EMAILS,
        email_plaintext_message=email_plaintext_message,
    )


def notify_us_that_landing_pool_blocked(
    user_email: str, landing_pool_id: int, product_source_id: int, message: str
) -> None:
    subject = "Trafficcino: User written to us"

    context = {
        "user_email": user_email,
        "message": message,
        "product_source_link": "https://{}/admin/landing/productsource/{}/change/".format(
            settings.HOST_NAME, product_source_id
        ),
    }
    email_plaintext_message = render_to_string(
        "email/landing_pool_blocked.txt", context
    )

    utils.send_letter(
        subject,
        to_email=settings.DEFAULT_ADMIN_EMAILS,
        email_plaintext_message=email_plaintext_message,
    )
