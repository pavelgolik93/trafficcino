from typing import Type, Union

from django.db import transaction
from rest_framework.exceptions import ValidationError

from user.models import User
from . import email
from .. import selectors
from ..models import LandingPool, ProductSource, LandingPoolSettings


def set_disable_status_for_all_user_landing_pools(user_id: int) -> None:
    _change_active_status_for_user_landing_pools(user_id, is_active=False)


def set_active_status_for_all_user_landing_pools(user_id: int) -> None:
    _change_active_status_for_user_landing_pools(user_id, is_active=True)


def _change_active_status_for_user_landing_pools(user_id: int, is_active: bool) -> None:
    LandingPool.objects.filter(user_id=user_id).update(is_active=is_active)


def landing_pool_set_step_analysis(user_id: Type[int] | int, landing_pool_id: int):
    _landing_pool_set_step(user_id, landing_pool_id, LandingPool.Steps.ANALYSIS)


def landing_pool_set_step_reviewing(user_id: Type[int] | int, landing_pool_id: int):
    _landing_pool_set_step(user_id, landing_pool_id, LandingPool.Steps.REVIEWING)


def landing_pool_set_step_block(user_id: Type[int] | int, landing_pool_id: int):
    _landing_pool_set_step(user_id, landing_pool_id, LandingPool.Steps.BLOCK)


def landing_pool_set_step_composition(user_id: Type[int] | int, landing_pool_id: int):
    _landing_pool_set_step(user_id, landing_pool_id, LandingPool.Steps.COMPOSITION)


def landing_pool_set_step_customization(user_id: Type[int] | int, landing_pool_id: int):
    _landing_pool_set_step(user_id, landing_pool_id, LandingPool.Steps.CUSTOMIZATION)


def landing_pool_set_step_complete(user_id: Type[int] | int, landing_pool_id: int):
    _landing_pool_set_step(user_id, landing_pool_id, LandingPool.Steps.COMPLETE)


def _landing_pool_set_step(
    user_id: Type[int] | int, landing_pool_id: int, step: LandingPool.Steps
):
    LandingPool.objects.filter(user_id=user_id, id=landing_pool_id).update(step=step)


def landing_pool_update_name(user_id: int, landing_pool_id: int, new_name: str):
    if new_name is None or isinstance(new_name, str) is False:
        raise ValidationError({"name": ["Bad type 'name' parameter"]})

    LandingPool.objects.filter(user_id=user_id, id=landing_pool_id).update(
        name=new_name
    )


@transaction.atomic()
def write_to_us(
    user: User, landing_pool_id: int, product_source_id: int, message: str
) -> None:
    landing_pool_set_step_block(user.id, landing_pool_id)
    email.notify_us_that_landing_pool_blocked(
        user.email, landing_pool_id, product_source_id, message
    )


def check_active_landings_in_relation_to_tariff_plan_limit(
    user_id: int, max_landings_amount: Union[int, None]
) -> None:
    match max_landings_amount:
        case None:
            set_active_status_for_all_user_landing_pools(user_id)
        case _:
            landing_pools = selectors.landing_pools_by_user(user_id)
            for i, landing_pool in enumerate(landing_pools):
                is_active = True if i < max_landings_amount else False
                landing_pool.is_active = is_active
                landing_pool.save()


def run_actions_after_landing_pool_create(landing_pool: LandingPool) -> None:
    create_landing_pool_settings(landing_pool.id)
    email.notify_us_that_created_new_landing_pool(landing_pool)


def create_landing_pool_settings(landing_pool_id: int):
    LandingPoolSettings.objects.create(landing_pool_id=landing_pool_id)


def landing_pool_set_step_analysis_or_composition(landing_pool: LandingPool) -> None:
    query = ProductSource.objects.filter(product__landing_pool=landing_pool)
    if query.filter(status=ProductSource.Statuses.ANALYSIS).exists() is True:
        landing_pool_set_step_analysis(landing_pool.user_id, landing_pool.id)
        email.notify_us_that_reanalyze_required(landing_pool)
        return
    if query.exclude(status=ProductSource.Statuses.ACCEPT).exists() is False:
        landing_pool_set_step_composition(landing_pool.user_id, landing_pool.id)
