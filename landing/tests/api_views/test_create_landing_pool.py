from unittest.mock import patch

from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITransactionTestCase

from landing.models import ProductSource, LandingPool
from user.models import User


class TestCreateLandingPool(APITransactionTestCase):
    fixtures = [
        "user/fixtures/user.json",
        "landing/fixtures/marketplace.json",
        "landing/fixtures/ad_network.json",
    ]
    reset_sequences = True

    def setUp(self) -> None:
        self.user = User.objects.get(id=1)
        self.client.force_authenticate(user=self.user)
        self.url = reverse("landing_private:creator:create")

    @patch("utils.email.send_letter")
    def test_create(self, *args, **kwargs):
        data = {
            "products": [
                {
                    "sources": [
                        {"sku": "1_1", "marketplace": 1},
                    ]
                },
                {
                    "sources": [
                        {"sku": "2_1", "marketplace": 3},
                    ]
                },
            ],
        }

        response = self.client.post(self.url, data=data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        landing_pool = LandingPool.objects.get(pk=1)
        self.assertEqual(landing_pool.step, LandingPool.Steps.ANALYSIS)
        self.assertEqual(hasattr(landing_pool, "settings"), True)

    @patch("utils.email.send_letter")
    def test_create_with_setting_id(self, *args, **kwargs):
        data = {
            "id": 100,
            "products": [
                {
                    "id": 110,
                    "sources": [
                        {"id": 111, "sku": "1_1", "marketplace": 1},
                    ],
                }
            ],
        }

        response = self.client.post(self.url, data=data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertNotEqual(response.data["id"], data["id"])
        for in_product, out_product in zip(data["products"], response.data["products"]):
            self.assertNotEqual(in_product["id"], out_product["id"])
            for in_source, out_source in zip(
                in_product["sources"], out_product["sources"]
            ):
                self.assertNotEqual(in_source["id"], out_source["id"])

    @patch("utils.email.send_letter")
    def test_create_with_setting_status(self, *args, **kwargs):
        data = {
            "products": [
                {
                    "sources": [
                        {"sku": "1_1", "marketplace": 1, "status": "not_found"},
                    ],
                }
            ],
        }
        response = self.client.post(self.url, data=data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(
            response.data["products"][0]["sources"][0]["status"],
            ProductSource.Statuses.ANALYSIS,
        )

    def test_create_with_empty_products(self):
        data = {
            "products": [],
        }

        response = self.client.post(self.url, data=data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_create_with_empty_product_sources(self):
        data = {
            "products": [
                {
                    "sources": [],
                },
            ]
        }

        response = self.client.post(self.url, data=data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
