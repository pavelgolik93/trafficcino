from unittest.mock import patch

from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITransactionTestCase

from landing.models import LandingPool, ProductSource, Image
from user.models import User


class TestUpdateLandingPool(APITransactionTestCase):
    fixtures = [
        "user/fixtures/user.json",
        "landing/fixtures/marketplace.json",
        "landing/fixtures/ad_network.json",
        "landing/fixtures/for_tests/test_landing_pool_update.json",
    ]
    reset_sequences = True

    def setUp(self) -> None:
        self.user = User.objects.get(id=1)
        self.client.force_authenticate(user=self.user)
        self.url = reverse("landing_private:creator:update", args=(1,))

    @patch("utils.email.send_letter")
    def test_update_and_setting_next_step(self, *args, **kwargs):
        input_data = {
            "products": [
                {
                    "id": 1,
                    "sources": [
                        # old status = complete
                        {"id": 1, "marketplace": 1, "sku": "1_1", "status": "accept"}
                    ],
                }
            ],
        }
        output_data = {
            "id": 1,
            "products": [
                {
                    "id": 1,
                    "sources": [
                        {"id": 1, "marketplace": 1, "sku": "1_1", "status": "accept"}
                    ],
                }
            ],
        }

        response = self.client.patch(self.url, data=input_data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertDictEqual(response.data, output_data)

        landing_pool = LandingPool.objects.get(pk=1)
        self.assertEqual(landing_pool.step, LandingPool.Steps.COMPOSITION)

    def test_update_with_statuses_not_equal_accept(self):
        input_data = {
            "products": [
                {
                    "id": 1,
                    "sources": [
                        {"id": 1, "marketplace": 1, "sku": "1_1", "status": ""}
                    ],
                }
            ],
        }
        bad_statuses = [
            ProductSource.Statuses.ANALYSIS,
            ProductSource.Statuses.COMPLETE,
            ProductSource.Statuses.NOT_FOUND,
            ProductSource.Statuses.DECLINE,
        ]

        for s in bad_statuses:
            input_data["products"][0]["sources"][0]["status"] = s
            response = self.client.patch(self.url, data=input_data)
            self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_update_with_new_product_source(self):
        input_data = {
            "products": [
                {
                    "id": 1,
                    "sources": [{"marketplace": 1, "sku": "1_1"}],
                }
            ],
        }

        response = self.client.patch(self.url, data=input_data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    @patch("utils.email.send_letter")
    def test_only_update_add_new_product_and_product_source(self, *args, **kwargs):
        input_data = {
            "only_update": True,
            "products": [
                {
                    "id": 1,
                    "sources": [
                        {"id": 1, "marketplace": 1, "sku": "1_1", "status": "accept"},
                        {"marketplace": 2, "sku": "1_2"},
                    ],
                },
                {
                    "sources": [
                        {"marketplace": 3, "sku": "2_1"},
                    ],
                },
            ],
        }
        output_data = {
            "id": 1,
            "products": [
                {
                    "id": 1,
                    "sources": [
                        {"id": 1, "marketplace": 1, "sku": "1_1", "status": "accept"},
                        {"id": 2, "marketplace": 2, "sku": "1_2", "status": "analysis"},
                    ],
                },
                {
                    "id": 2,
                    "sources": [
                        {"id": 3, "marketplace": 3, "sku": "2_1", "status": "analysis"}
                    ],
                },
            ],
        }

        response = self.client.patch(self.url, data=input_data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertDictEqual(response.data, output_data)

        landing_pool = LandingPool.objects.get(pk=1)
        self.assertEqual(landing_pool.step, LandingPool.Steps.ANALYSIS)

    @patch("utils.email.send_letter")
    def test_only_update_change_status_to_decline_with_changes(self, *args, **kwargs):
        input_data = {
            "only_update": True,
            "products": [
                {
                    "id": 1,
                    "sources": [
                        {"id": 1, "marketplace": 1, "sku": "1_11", "status": "decline"},
                    ],
                }
            ],
        }
        output_data = {
            "id": 1,
            "products": [
                {
                    "id": 1,
                    "sources": [
                        {
                            "id": 1,
                            "marketplace": 1,
                            "sku": "1_11",
                            "status": "analysis",
                        },
                    ],
                },
            ],
        }

        response = self.client.patch(self.url, data=input_data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertDictEqual(response.data, output_data)

        landing_pool = LandingPool.objects.get(pk=1)
        self.assertEqual(landing_pool.step, LandingPool.Steps.ANALYSIS)

        product_source = ProductSource.objects.get(pk=1)
        self.assertEqual(product_source.link, "")
        self.assertEqual(product_source.title, "")
        self.assertEqual(product_source.price, None)

        product_source_images_count = Image.objects.filter(product_source_id=1).count()
        self.assertEqual(product_source_images_count, 0)

    @patch("utils.email.send_letter")
    def test_only_update_change_status_to_decline_without_changes(
        self, *args, **kwargs
    ):
        input_data = {
            "only_update": True,
            "products": [
                {
                    "id": 1,
                    "sources": [
                        {"id": 1, "marketplace": 1, "sku": "1_1", "status": "decline"},
                    ],
                }
            ],
        }
        output_data = {
            "id": 1,
            "products": [
                {
                    "id": 1,
                    "sources": [
                        {"id": 1, "marketplace": 1, "sku": "1_1", "status": "decline"},
                    ],
                },
            ],
        }

        response = self.client.patch(self.url, data=input_data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertDictEqual(response.data, output_data)

        landing_pool = LandingPool.objects.get(pk=1)
        self.assertEqual(landing_pool.step, LandingPool.Steps.REVIEWING)

        product_source_images_count = Image.objects.filter(product_source_id=1).count()
        self.assertEqual(product_source_images_count, 3)
