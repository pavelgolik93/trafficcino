from django.urls import path, include

from . import views


app_name = "landing_private"

creator_patterns = [
    path("create/", views.LandingPoolCreateAPI.as_view(), name="create"),
    path(
        "<int:landing_pool_id>/",
        include(
            [
                path("", views.LandingPoolRetrieveAPI.as_view(), name="detail"),
                path("update/", views.LandingPoolUpdateAPI.as_view(), name="update"),
                path(
                    "landing/data/preview/",
                    views.LandingPreviewAPI.as_view(),
                    name="landing-preview",
                ),
                path(
                    "products/",
                    include(
                        [
                            path(
                                "",
                                views.ProductListAPI.as_view(),
                                name="products-list",
                            ),
                            path(
                                "update/",
                                views.ProductListUpdateAPI.as_view(),
                                name="products-list-update",
                            ),
                            path(
                                "<int:product_id>/",
                                include(
                                    [
                                        path(
                                            "titles/",
                                            views.ProductTitlesAPI.as_view(),
                                            name="products-titles",
                                        ),
                                        path(
                                            "images/create/",
                                            views.ProductImagesCreateAPI.as_view(),
                                            name="product-images-create",
                                        ),
                                        path(
                                            "image/<int:image_id>/delete/",
                                            views.ProductImageDeleteAPI.as_view(),
                                            name="product-image-delete",
                                        ),
                                    ]
                                ),
                            ),
                        ],
                    ),
                ),
                path(
                    "settings/",
                    views.LandingPoolSettingsRetrieveAPI.as_view(),
                    name="settings",
                ),
                path(
                    "settings/update/",
                    views.LandingPoolSettingsUpdateAPI.as_view(),
                    name="settings-update",
                ),
                path(
                    "source/<int:product_source_id>/reviewing/request/",
                    views.WriteToUsAPI.as_view(),
                    name="reviewing-request",
                ),
            ]
        ),
    ),
]

landing_pages_patterns = [
    path("", views.LandingPoolListAPI.as_view(), name="list"),
    path(
        "unfinished/",
        views.UnfinishedLandingPoolListAPI.as_view(),
        name="list-unfinished",
    ),
    path(
        "<int:landing_pool_id>/update/name/",
        views.landing_pool_update_name,
        name="landing-page-update-name",
    ),
]

urlpatterns = [
    path("overview/", views.OverviewAPI.as_view(), name="overview"),
    path("pools/", include((creator_patterns, "creator"))),
    path("pages/", include((landing_pages_patterns, "landing-pages"))),
    path(
        "marketplace/create/",
        views.MarketplaceCreateAPI.as_view(),
        name="marketplace-create",
    ),
    path("options/", views.options),
]
