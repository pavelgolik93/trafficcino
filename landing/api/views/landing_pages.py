from rest_framework import views, status
from rest_framework.decorators import api_view
from rest_framework.response import Response

from .. import serializers
from ... import selectors, services


class LandingPoolListAPI(views.APIView):
    serializer_class = serializers.LandingPoolListSerializer

    def get(self, request):
        landing_pools = selectors.finished_landing_pools_by_user(
            user_id=request.user.id
        )

        serializer = self.serializer_class(instance=landing_pools, many=True)
        return Response(data=serializer.data)


class UnfinishedLandingPoolListAPI(views.APIView):
    serializer_class = serializers.UnfinishedLandingPoolListSerializer

    def get(self, request):
        landing = selectors.unfinished_landing_pools_by_user(user_id=request.user.id)

        serializer = self.serializer_class(instance=landing, many=True)
        return Response(data=serializer.data)


@api_view(["POST"])
def landing_pool_update_name(request, landing_pool_id):
    new_name = request.data.get("name")

    services.landing_pool_update_name(request.user.id, landing_pool_id, new_name)
    return Response(status=status.HTTP_200_OK)
