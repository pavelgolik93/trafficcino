from rest_framework import views, generics
from rest_framework.decorators import api_view
from rest_framework.permissions import AllowAny
from rest_framework.response import Response

from app.mixins import RetrieveAPIMixin
from .. import serializers
from ... import selectors
from ...models import ProductSource, LandingPool, Landing


class LandingDetailAPI(RetrieveAPIMixin, views.APIView):
    permission_classes = (AllowAny,)

    serializer_class = serializers.LandingSerializer
    selector_function = staticmethod(selectors.landing_get)

    def get(self, request, landing_id):
        landing: Landing = self.get_object(landing_id=landing_id)

        serializer = self.serializer_class(instance=landing.landing_pool)
        return Response(data=serializer.data)


class MarketplaceCreateAPI(generics.CreateAPIView):
    serializer_class = serializers.MarketplaceCreateSerializer


@api_view()
def options(request):
    marketplace_serializer = serializers.MarketplaceSerializer(
        instance=selectors.all_marketplaces_with_user(request.user.id), many=True
    )
    ad_network_serializer = serializers.AdNetworkSerializer(
        instance=selectors.ad_networks(), many=True
    )

    return Response(
        {
            "marketplaces": {m["id"]: m for m in marketplace_serializer.data},
            "ad_networks": {an["id"]: an for an in ad_network_serializer.data},
            "statuses": ProductSource.Statuses.values,
            "steps_creating": LandingPool.Steps.values,
        }
    )
