from rest_framework import views
from rest_framework.response import Response

from analytics import services as analytics_services


class OverviewAPI(views.APIView):
    def get(self, request):
        insights = analytics_services.Insights(user_id=request.user.id)
        data = insights.overview_analytics()
        return Response(data)
