from django.http import Http404
from rest_framework import views, generics, status
from rest_framework.response import Response

from app.mixins import RetrieveAPIMixin
from ... import serializers
from .... import selectors, services
from ....models import LandingPool


class LandingPoolCreateAPI(generics.CreateAPIView):
    serializer_class = serializers.LandingPoolInputSerializer

    def perform_create(self, serializer):
        instance = serializer.save()
        services.run_actions_after_landing_pool_create(instance)


class LandingPoolUpdateAPI(RetrieveAPIMixin, generics.GenericAPIView):
    serializer_class = serializers.LandingPoolInputSerializer
    selector_function = staticmethod(selectors.landing_pool_by_user)

    def get_object(self, *args, **kwargs):
        landing_pool = super().get_object(*args, **kwargs)
        if landing_pool.step != LandingPool.Steps.REVIEWING:
            raise Http404
        return landing_pool

    def patch(self, request, landing_pool_id):
        only_update = request.data.pop("only_update", False)
        landing_pool = self.get_object(
            user_id=request.user.id, landing_pool_id=landing_pool_id
        )

        serializer = self.get_serializer(
            instance=landing_pool,
            data=request.data,
            context=dict(only_update=only_update),
        )
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)

        return Response(data=serializer.data)

    def perform_update(self, serializer):
        instance = serializer.save()
        services.landing_pool_set_step_analysis_or_composition(instance)


class LandingPoolRetrieveAPI(RetrieveAPIMixin, views.APIView):
    serializer_class = serializers.LandingPoolRetrieveSerializer
    selector_function = staticmethod(selectors.landing_pool_by_user)

    def get(self, request, landing_pool_id):
        landing_pool = self.get_object(
            user_id=request.user.id, landing_pool_id=landing_pool_id
        )

        serializer = self.serializer_class(instance=landing_pool)
        return Response(data=serializer.data)


class WriteToUsAPI(generics.GenericAPIView):
    serializer_class = serializers.WriteToUsCreateSerializer

    def post(self, request, landing_pool_id: int, product_source_id: int):
        serializer = self.get_serializer(
            data={
                **request.data,
                "landing_pool_id": landing_pool_id,
                "product_source_id": product_source_id,
            }
        )
        serializer.is_valid(raise_exception=True)

        services.write_to_us(user=request.user, **serializer.validated_data)
        return Response(status=status.HTTP_200_OK)
