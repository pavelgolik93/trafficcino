from django.http import Http404
from rest_framework import views, generics, status
from rest_framework.response import Response

from ... import serializers
from .... import selectors, services


class ProductListAPI(views.APIView):
    serializer_class = serializers.CompositionProductListSerializer

    def get(self, request, landing_pool_id):
        products = selectors.landing_pool_products_by_user(
            request.user.id, landing_pool_id
        )
        serializer = self.serializer_class(instance=products, many=True)
        return Response(data=serializer.data)


class ProductListUpdateAPI(generics.GenericAPIView):
    serializer_class = serializers.CompositionProductListUpdateSerializer

    def put(self, request, landing_pool_id):
        products = selectors.landing_pool_products_by_user(
            request.user.id, landing_pool_id
        )

        serializer = self.serializer_class(
            instance=products, data=request.data, many=True
        )
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)

        return Response(data=serializer.data)

    def perform_update(self, serializer):
        serializer.save()

        services.landing_pool_set_step_customization(
            self.request.user.id, self.kwargs["landing_pool_id"]
        )


class ProductTitlesAPI(views.APIView):
    def get(self, request, landing_pool_id, product_id):
        instance = selectors.products_sources_by_user_landing_pool_and_product_id(
            request.user.id, landing_pool_id, product_id
        )
        return Response(
            data=sorted(filter(None, instance.values_list("title", flat=True)))
        )


class ProductImagesCreateAPI(generics.CreateAPIView):
    serializer_class = serializers.ImageCreateSerializer

    def get_serializer_context(self):
        return {
            "request": self.request,
            "format": self.format_kwarg,
            "view": self,
            "landing_pool_id": self.kwargs["landing_pool_id"],
        }

    def create(self, request, *args, **kwargs):
        data = [
            {"src": img, "product": kwargs["product_id"]}
            for img in request.FILES.getlist("images")
        ]

        serializer = self.get_serializer(data=data, many=True)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        return Response(serializer.data, status=status.HTTP_201_CREATED)


class ProductImageDeleteAPI(views.APIView):
    serializer_class = serializers.ImageCreateSerializer

    def delete(self, request, landing_pool_id, product_id, image_id):
        is_destroyed = services.product_image_destroy(
            user_id=request.user.id,
            image_id=image_id,
            landing_pool_id=landing_pool_id,
            product_id=product_id,
        )
        if is_destroyed is False:
            raise Http404
        return Response(status=status.HTTP_200_OK)
