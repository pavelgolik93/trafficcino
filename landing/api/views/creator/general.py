from rest_framework import views
from rest_framework.response import Response

from app.mixins import RetrieveAPIMixin
from landing import selectors
from landing.api import serializers


class LandingPreviewAPI(RetrieveAPIMixin, views.APIView):
    serializer_class = serializers.LandingSerializer
    selector_function = staticmethod(selectors.fro_private_preview_landing_get)

    def get(self, request, landing_pool_id):
        landing_pool = self.get_object(landing_pool_id=landing_pool_id)

        serializer = self.serializer_class(instance=landing_pool)
        return Response(data=serializer.data)
