from rest_framework import views, status
from rest_framework.response import Response

from app.mixins import RetrieveAPIMixin
from ... import serializers
from .... import selectors, services


class LandingPoolSettingsRetrieveAPI(RetrieveAPIMixin, views.APIView):
    serializer_class = serializers.LandingPoolSettingsSerializer
    selector_function = staticmethod(selectors.landing_pool_settings_by_user)

    def get(self, request, landing_pool_id):
        landing_pool_settings = self.get_object(
            user_id=request.user.id, landing_pool_id=landing_pool_id
        )

        serializer = self.serializer_class(instance=landing_pool_settings)
        return Response(data=serializer.data)


class LandingPoolSettingsUpdateAPI(RetrieveAPIMixin, views.APIView):
    serializer_class = serializers.LandingPoolSettingsUpdateSerializer
    selector_function = staticmethod(selectors.landing_pool_settings_by_user)

    def patch(self, request, landing_pool_id):
        landing_pool_settings = self.get_object(
            user_id=request.user.id, landing_pool_id=landing_pool_id
        )

        serializer = self.serializer_class(
            data=request.data, instance=landing_pool_settings
        )
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)

        return Response(status=status.HTTP_200_OK)

    def perform_update(self, serializer):
        instance = serializer.save()

        services.create_landings_by_selected_ad_networks(instance)
        services.landing_pool_set_step_complete(
            self.request.user.id, self.kwargs["landing_pool_id"]
        )
