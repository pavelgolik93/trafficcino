from django.urls import path

from . import views


app_name = "landing_public"

urlpatterns = [
    path(
        "page/<uuid:landing_id>/", views.LandingDetailAPI.as_view(), name="landing-page"
    ),
]
