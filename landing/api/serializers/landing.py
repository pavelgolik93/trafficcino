from urllib.parse import urljoin

from colorfield.serializers import ColorField
from django.conf import settings
from rest_framework import serializers, fields

from .marketplace import MarketplaceSerializer


__all__ = ["LandingSerializer", "LandingListSerializer"]


class ProductSourceSerializer(serializers.Serializer):
    id = fields.IntegerField()
    marketplace = MarketplaceSerializer()
    link = fields.URLField()
    price = fields.DecimalField(max_digits=10, decimal_places=2)
    in_stock = fields.BooleanField()


class ProductSerializer(serializers.Serializer):
    title = fields.CharField()
    images = serializers.SerializerMethodField()
    sources = ProductSourceSerializer(many=True)

    def get_images(self, instance):
        return [img.src.url for img in instance.product_images.all()]


class LandingSettingsSerializer(serializers.Serializer):
    show_price = fields.BooleanField()
    show_on_stock = fields.BooleanField()
    collect_phones = fields.BooleanField()
    collect_phones_motivation_text = fields.CharField()
    collect_emails = fields.BooleanField()
    collect_emails_motivation_text = fields.CharField()
    background_color = ColorField()
    text_color = ColorField()
    button_color = ColorField()
    text_button_color = ColorField()


class LandingSerializer(serializers.Serializer):
    settings = LandingSettingsSerializer()
    products = ProductSerializer(many=True)


class LandingProductSourceListSerializer(serializers.Serializer):
    id = fields.IntegerField()
    marketplace = serializers.PrimaryKeyRelatedField(read_only=True)


class LandingProductListSerializer(serializers.Serializer):
    id = fields.IntegerField()
    title = fields.CharField()
    image = serializers.SerializerMethodField()
    sources = LandingProductSourceListSerializer(many=True)

    def get_image(self, instance):
        img = (
            instance.product_images.filter(is_selected=True)
            .order_by("sort_index")
            .first()
        )
        if img is not None:
            return img.src.url
        return None


class LandingListSerializer(serializers.Serializer):
    id = fields.UUIDField()
    ad_network = serializers.PrimaryKeyRelatedField(read_only=True)
    link = serializers.SerializerMethodField()
    products = LandingProductListSerializer(source="landing_pool.products", many=True)

    def get_link(self, instance):
        return urljoin(
            "https://{}".format(settings.LANDING_HOST_NAME), str(instance.id)
        )
