from rest_framework import serializers, fields
from rest_framework.exceptions import ValidationError

from ... import services
from ...models import Marketplace, ProductSource


class ProductSourceSerializer(serializers.Serializer):
    id = fields.IntegerField()
    marketplace = serializers.PrimaryKeyRelatedField(read_only=True)
    sku = fields.CharField()
    link = fields.URLField()
    title = fields.CharField()
    price = fields.DecimalField(max_digits=10, decimal_places=2)
    images = serializers.SerializerMethodField()
    in_stock = fields.BooleanField()
    status = fields.CharField()
    created_at = fields.DateTimeField()

    def get_images(self, instance):
        return [v.src.url for v in instance.product_source_images.all()]


class ProductSourceInputSerializer(serializers.ModelSerializer):
    sku = fields.CharField(max_length=50)
    marketplace = serializers.PrimaryKeyRelatedField(queryset=Marketplace.objects.all())
    status = fields.ChoiceField(
        choices=ProductSource.Statuses.choices, default=ProductSource.Statuses.ANALYSIS
    )

    class Meta:
        model = ProductSource
        fields = (
            "id",
            "sku",
            "marketplace",
            "status",
        )

    @property
    def only_update(self):
        return self.context.get("only_update", None)

    def validate_status(self, value):
        if self.only_update is False and value != ProductSource.Statuses.ACCEPT:
            raise ValidationError("Value must be 'accept'")

        if self.instance:
            if self.only_update is True:
                self.validate_status_transition(value)

                if value == ProductSource.Statuses.DECLINE:
                    is_changed = self.validate_decline_data()
                    if is_changed:
                        value = ProductSource.Statuses.ANALYSIS

        if self.instance is None:
            value = ProductSource.Statuses.ANALYSIS

        return value

    def validate_status_transition(self, value):
        if self.instance.status != value:
            if self.instance.status == ProductSource.Statuses.NOT_FOUND:
                raise ValidationError("Value 'not_found' can't be changed")

            allowed_statuses = [
                ProductSource.Statuses.ACCEPT,
                ProductSource.Statuses.DECLINE,
            ]
            if (
                self.instance.status == ProductSource.Statuses.COMPLETE
                and value not in allowed_statuses
            ):
                raise ValidationError(
                    "Value 'complete' must be changed to 'accept' or 'decline'"
                )

    def validate_decline_data(self):
        is_changed = False
        sku = self.initial_data["sku"]
        marketplace: int = self.initial_data["marketplace"]

        if self.instance.sku != sku or self.instance.marketplace.id != marketplace:
            is_changed = True
        return is_changed

    def update(self, instance, validated_data):
        instance = super().update(instance, validated_data)
        if instance.status == ProductSource.Statuses.ANALYSIS:
            services.product_source_clean_extra_data(instance.id)
        return instance
