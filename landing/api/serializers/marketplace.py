from urllib.parse import urlparse

from django.conf import settings
from rest_framework import serializers, fields

from ...models import Marketplace


class MarketplaceNameDefault(serializers.CurrentUserDefault):
    def __call__(self, serializer_field):
        return urlparse(serializer_field.parent.initial_data["url"]).hostname


class MarketplaceSerializer(serializers.ModelSerializer):
    user = fields.HiddenField(default=serializers.CurrentUserDefault())
    name = fields.CharField(default=MarketplaceNameDefault())
    logo = serializers.SerializerMethodField()

    class Meta:
        model = Marketplace
        fields = ("id", "user", "name", "logo", "url")

    def get_logo(self, instance):
        if instance.user is not None:
            return "{}ailab/public/trafficcino/landing/marketplaces/user_custom_marketplace.svg".format(
                settings.MEDIA_URL
            )
        return instance.logo.url


class MarketplaceCreateSerializer(MarketplaceSerializer):
    pass
