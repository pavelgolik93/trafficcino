from rest_framework import serializers, fields
from rest_framework.exceptions import ValidationError

from landing import services


class WriteToUsCreateSerializer(serializers.Serializer):
    landing_pool_id = fields.IntegerField()
    product_source_id = fields.IntegerField()
    message = fields.CharField()

    def validate(self, attrs):
        product_source_exists = services.landing_pool_product_source_by_user_exist(
            user_id=self.context["request"].user.id,
            landing_pool_id=attrs["landing_pool_id"],
            product_source_id=attrs["product_source_id"],
        )
        if product_source_exists is False:
            raise ValidationError("Bad lookup values.")
        return attrs
