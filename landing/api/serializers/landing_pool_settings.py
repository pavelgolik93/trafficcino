from colorfield.serializers import ColorField
from rest_framework import serializers, fields

from ...models import AdNetwork, LandingPoolSettings


class LandingPoolSettingsSerializer(serializers.Serializer):
    ad_networks = serializers.PrimaryKeyRelatedField(read_only=True, many=True)
    show_price = fields.BooleanField()
    show_on_stock = fields.BooleanField()
    collect_phones = fields.BooleanField()
    collect_phones_motivation_text = fields.CharField()
    collect_emails = fields.BooleanField()
    collect_emails_motivation_text = fields.CharField()
    background_color = ColorField()
    text_color = ColorField()
    button_color = ColorField()
    text_button_color = ColorField()


class LandingPoolSettingsUpdateSerializer(serializers.ModelSerializer):
    ad_networks = serializers.PrimaryKeyRelatedField(
        queryset=AdNetwork.objects.all(), many=True, required=True
    )
    show_price = fields.BooleanField()
    show_on_stock = fields.BooleanField()
    collect_phones = fields.BooleanField()
    collect_phones_motivation_text = fields.CharField(allow_blank=True)
    collect_emails = fields.BooleanField()
    collect_emails_motivation_text = fields.CharField(allow_blank=True)
    background_color = ColorField()
    text_color = ColorField()
    button_color = ColorField()
    text_button_color = ColorField()

    class Meta:
        model = LandingPoolSettings
        fields = (
            "ad_networks",
            "show_price",
            "show_on_stock",
            "collect_phones",
            "collect_phones_motivation_text",
            "collect_emails",
            "collect_emails_motivation_text",
            "background_color",
            "text_color",
            "button_color",
            "text_button_color",
        )
