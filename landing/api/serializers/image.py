from rest_framework import serializers, fields
from rest_framework.exceptions import ValidationError

from ... import services
from ...models import Image, Product


class ImageListSerializer(serializers.Serializer):
    id = fields.IntegerField()
    product = serializers.PrimaryKeyRelatedField(read_only=True)
    product_source = serializers.PrimaryKeyRelatedField(read_only=True)
    src = fields.ImageField()
    is_selected = fields.BooleanField()
    sort_index = fields.IntegerField()


class ImageUpdateSerializer(serializers.ModelSerializer):
    id = fields.IntegerField()
    is_selected = fields.BooleanField()
    sort_index = fields.IntegerField()

    class Meta:
        model = Image
        fields = ("id", "is_selected", "sort_index")


class ImageCreateSerializer(serializers.ModelSerializer):
    product = serializers.PrimaryKeyRelatedField(queryset=Product.objects.all())
    product_source = serializers.PrimaryKeyRelatedField(read_only=True)
    src = serializers.ImageField()
    is_selected = fields.BooleanField(default=False)
    sort_index = fields.IntegerField(default=1000)

    class Meta:
        model = Image
        fields = (
            "id",
            "product",
            "product_source",
            "src",
            "is_selected",
            "sort_index",
        )

    def validate(self, attrs):
        product_exists = services.landing_pool_product_by_user_exist(
            user_id=self.context["request"].user.id,
            product_id=attrs["product"].id,
            landing_pool_id=self.context["landing_pool_id"],
        )
        if product_exists is False:
            raise ValidationError("Bad lookup values.")
        return attrs
