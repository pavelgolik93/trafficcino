from rest_framework import serializers, fields
from rest_framework.exceptions import ValidationError

from .landing import LandingListSerializer
from .product import ProductListSerializer, ProductInputSerializer
from ...mixins import NestedUpdateMixin, NestedCreateMixin
from ...models import LandingPool


class LandingPoolListSerializer(serializers.Serializer):
    id = fields.IntegerField()
    name = fields.CharField()
    landings = LandingListSerializer(many=True)


class LandingPoolRetrieveSerializer(serializers.Serializer):
    step = fields.CharField()
    products = ProductListSerializer(many=True)


class LandingPoolInputSerializer(
    NestedCreateMixin, NestedUpdateMixin, serializers.ModelSerializer
):
    user = fields.HiddenField(
        default=serializers.CreateOnlyDefault(default=serializers.CurrentUserDefault())
    )
    name = fields.HiddenField(default=serializers.CreateOnlyDefault(default="Landing"))
    products = ProductInputSerializer(many=True)

    class Meta:
        model = LandingPool
        fields = ("id", "user", "name", "products")

    def validate_products(self, value):
        if not value:
            raise ValidationError("This field may not be blank.")
        return value


class UnfinishedLandingPoolListSerializer(serializers.Serializer):
    id = fields.IntegerField()
    step = fields.CharField()
    name = fields.CharField()
    created_at = fields.DateTimeField()
