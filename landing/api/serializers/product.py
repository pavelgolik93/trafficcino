from collections import defaultdict, OrderedDict

from rest_framework import serializers, fields
from rest_framework.exceptions import ValidationError
from rest_framework.fields import empty

from .image import ImageListSerializer, ImageUpdateSerializer
from .product_source import ProductSourceSerializer, ProductSourceInputSerializer
from ...mixins import NestedUpdateMixin, NestedOnlyUpdateMixin, NestedCreateMixin
from ...models import Product, Image


class ProductListSerializer(serializers.Serializer):
    id = fields.IntegerField()
    sources = ProductSourceSerializer(many=True)


class ProductInputSerializer(
    NestedCreateMixin, NestedUpdateMixin, serializers.ModelSerializer
):
    sources = ProductSourceInputSerializer(many=True)

    class Meta:
        model = Product
        fields = ("id", "sources")

    def validate_sources(self, value):
        if not value:
            raise ValidationError("This field may not be blank.")
        return value


class CustomListSerializerClass(serializers.ListSerializer):
    def update(self, instances, validated_data):
        instance_mapping = {instance.id: instance for instance in instances}
        data_mapping = {data["id"]: data for data in validated_data}

        ret = []
        for product_id, data in data_mapping.items():
            instance = instance_mapping.get(product_id)
            if instance is not None:
                ret.append(self.child.update(instance, data))

        return ret


class CompositionProductListSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    title = serializers.SerializerMethodField()
    sort_index = fields.IntegerField()
    images = ImageListSerializer(source="product_images", many=True)

    def get_title(self, instance):
        value = "Enter your product title."
        product_source = instance.sources.first()
        if product_source is not None and product_source.title:
            value = product_source.title
        return value


class CompositionProductListUpdateSerializer(
    NestedOnlyUpdateMixin, serializers.ModelSerializer
):
    id = serializers.IntegerField()
    title = fields.CharField(max_length=300)
    sort_index = fields.IntegerField()
    images = ImageUpdateSerializer(source="product_images", many=True)

    class Meta:
        model = Product
        list_serializer_class = CustomListSerializerClass
        fields = (
            "id",
            "title",
            "sort_index",
            "images",
        )

    def __init__(self, *args, **kwargs):
        self._updated_instance = None
        self._save_kwargs = defaultdict(dict, {})

        if "data" in kwargs:
            data = kwargs["data"]

            if not isinstance(data, list):
                raise ValidationError(
                    {
                        "non_field_errors": [
                            "Invalid data. Expected a list, but got {datatype}.".format(
                                datatype=type(data).__name__
                            )
                        ]
                    }
                )

            kwargs["data"] = self._set_sort_index(data)

        super().__init__(*args, **kwargs)

    def _set_sort_index(self, data):
        for i, product in enumerate(data):
            product[Product.sort_index.field.name] = i
            for j, img in enumerate(product.get("images", [])):
                img[Image.sort_index.field.name] = j
        return data

    def get_initial(self):
        initial_data = self.initial_data
        for data in initial_data:
            if data["id"] == self._updated_instance.id:
                initial_data = data
                break

        return OrderedDict(
            [
                (field_name, field.get_value(initial_data))
                for field_name, field in self.fields.items()
                if (field.get_value(initial_data) is not empty) and not field.read_only
            ]
        )

    def update(self, instance, validated_data):
        self._updated_instance = instance
        return super().update(instance, validated_data)
