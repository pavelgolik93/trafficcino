from rest_framework import serializers, fields


class AdNetworkSerializer(serializers.Serializer):
    id = fields.IntegerField()
    name = fields.CharField()
    logo = fields.ImageField()
