from django.db import models
from django.utils import timezone
from django.utils.translation import gettext_lazy as _

from app.fields import CompressedImageField
from app.storage import AWSStorage
from landing.utils import product_image_upload_to


class Product(models.Model):
    landing_pool = models.ForeignKey(
        "landing.LandingPool",
        verbose_name=_("landing pool"),
        related_name="products",
        on_delete=models.CASCADE,
    )
    title = models.CharField(_("title"), max_length=300, blank=True)
    sort_index = models.IntegerField(_("sort index"), default=1000)
    created_at = models.DateTimeField(_("created at"), default=timezone.now)


class ProductSource(models.Model):
    class Statuses(models.TextChoices):
        ANALYSIS = "analysis", _("Analysis")
        COMPLETE = "complete", _("Complete")
        NOT_FOUND = "not_found", _("Not found")
        ACCEPT = "accept", _("Accept")
        DECLINE = "decline", _("Decline")

    product = models.ForeignKey(
        "landing.Product",
        verbose_name=_("product"),
        related_name="sources",
        on_delete=models.CASCADE,
    )
    marketplace = models.ForeignKey(
        "landing.Marketplace", verbose_name=_("marketplace"), on_delete=models.PROTECT
    )
    sku = models.CharField(_("sku"), max_length=50)
    sort_index = models.IntegerField(_("sort index"), default=1000)
    status = models.CharField(
        choices=Statuses.choices, max_length=10, default=Statuses.ANALYSIS
    )

    # extra data
    link = models.URLField(blank=True)
    title = models.CharField(_("title"), max_length=300, blank=True)
    price = models.DecimalField(
        _("price"), max_digits=10, decimal_places=2, blank=True, null=True
    )
    in_stock = models.BooleanField(_("in stock"), default=True)
    created_at = models.DateTimeField(_("created at"), default=timezone.now)

    def __str__(self):
        return "{} ({})".format(self.marketplace.name, self.sku)


class Image(models.Model):
    product = models.ForeignKey(
        "landing.Product",
        verbose_name=_("product"),
        related_name="product_images",
        on_delete=models.CASCADE,
    )
    product_source = models.ForeignKey(
        "landing.ProductSource",
        verbose_name=_("product source"),
        related_name="product_source_images",
        on_delete=models.CASCADE,
        null=True,
        blank=True,
    )
    src = CompressedImageField(
        verbose_name=_("src"),
        max_width=350,
        upload_to=product_image_upload_to,
        storage=AWSStorage(),
    )
    is_selected = models.BooleanField(_("selected"), default=False)
    sort_index = models.IntegerField(_("sort index"), default=1000)
    created_at = models.DateTimeField(_("created at"), default=timezone.now)

    @property
    def base_folder_name(self):
        return "product_images"
