import uuid

from colorfield.fields import ColorField
from django.conf import settings
from django.db import models
from django.utils import timezone
from django.utils.translation import gettext_lazy as _


class LandingPool(models.Model):
    class Steps(models.TextChoices):
        ANALYSIS = ("analysis", "Analysis")
        REVIEWING = ("reviewing", "Reviewing")
        BLOCK = ("block", "Block")
        COMPOSITION = ("composition", "Composition")
        CUSTOMIZATION = ("customization", "Customization")
        COMPLETE = ("complete", "Complete")

    user = models.ForeignKey(
        settings.AUTH_USER_MODEL, verbose_name=_("user"), on_delete=models.CASCADE
    )
    name = models.CharField(max_length=50)
    is_active = models.BooleanField(default=True)
    step = models.CharField(
        choices=Steps.choices, max_length=15, default=Steps.ANALYSIS
    )
    created_at = models.DateTimeField(_("created at"), default=timezone.now)

    class Meta:
        indexes = [
            models.Index(fields=["user_id"]),
        ]

    def __str__(self):
        return f"({self.id}) {self.name}"


class Landing(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    landing_pool = models.ForeignKey(
        "landing.LandingPool",
        verbose_name=_("landing pool"),
        related_name="landings",
        on_delete=models.CASCADE,
    )
    ad_network = models.ForeignKey(
        "landing.AdNetwork", verbose_name=_("ad network"), on_delete=models.PROTECT
    )
    is_active = models.BooleanField(_("active"), default=True)
    scripts = models.TextField(_("head section scripts"), blank=True)
    created_at = models.DateTimeField(_("created at"), default=timezone.now)

    class Meta:
        unique_together = ("landing_pool", "ad_network")

    @property
    def landing_link(self):
        return "https://{}/{}".format(
            getattr(settings, "LANDING_HOST_NAME", None), self.id
        )

    @property
    def landing_preview_link(self):
        return self.landing_link + "?mode=preview"


class LandingPoolSettings(models.Model):
    landing_pool = models.OneToOneField(
        LandingPool,
        verbose_name=_("landing pool"),
        related_name="settings",
        on_delete=models.CASCADE,
        primary_key=True,
    )
    ad_networks = models.ManyToManyField(
        "landing.AdNetwork", verbose_name=_("ad networks"), blank=True
    )
    show_price = models.BooleanField(_("show price"), default=False)
    show_on_stock = models.BooleanField(_("show on stock"), default=False)
    collect_phones = models.BooleanField(_("collect phones"), default=False)
    collect_phones_motivation_text = models.TextField(
        _("motivation text for collecting phones"), blank=True
    )
    collect_emails = models.BooleanField(_("collect emails"), default=False)
    collect_emails_motivation_text = models.TextField(
        _("motivation text for collecting emails"), blank=True
    )
    background_color = ColorField(_("background color"), default="#F9FBFF")
    text_color = ColorField(_("text color"), default="#000000")
    button_color = ColorField(_("button color"), default="#053354")
    text_button_color = ColorField(_("text button color"), default="#FFFFFF")
