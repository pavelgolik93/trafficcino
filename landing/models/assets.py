from django.conf import settings
from django.core.validators import FileExtensionValidator
from django.db import models
from django.utils import timezone
from django.utils.translation import gettext_lazy as _

from app.storage import AWSStorage
from landing.utils import upload_to


class BaseAsset(models.Model):
    is_active = models.BooleanField(_("active"), default=True)
    name = models.CharField(max_length=50)
    logo = models.FileField(
        help_text=_("Accepts files with the .svg extension only"),
        validators=[FileExtensionValidator(["svg"])],
        upload_to=upload_to,
        storage=AWSStorage(),
        null=True,
    )
    created_at = models.DateTimeField(_("created at"), default=timezone.now)

    class Meta:
        abstract = True

    def __str__(self):
        return f"{'✅' if self.is_active else '❌'} {self.name}"


class Marketplace(BaseAsset):
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        verbose_name=_("user"),
        help_text=_("if a user wants to add their own marketplace."),
        on_delete=models.CASCADE,
        null=True,
        blank=True,
    )
    url = models.URLField(_("marketplace link"))

    @property
    def base_folder_name(self):
        return "marketplaces"


class AdNetwork(BaseAsset):
    @property
    def base_folder_name(self):
        return "ad_networks"
