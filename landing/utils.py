import os
from uuid import uuid4

from django.conf import settings


def hashing_filename(filename):
    name, file_extension = os.path.splitext(filename)
    return "{}{}".format(uuid4().hex, file_extension)


def upload_to(instance, filename):
    return "{}/landing/{}/{}".format(
        settings.AWS_MAIN_FOLDER, instance.base_folder_name, hashing_filename(filename)
    )


def product_image_upload_to(instance, filename):
    folder = instance.product_id
    if instance.product_source_id:
        folder = "{}_{}".format(instance.product_id, instance.product_source_id)

    return "{}/landing/{}/{}/{}".format(
        settings.AWS_MAIN_FOLDER,
        instance.base_folder_name,
        folder,
        hashing_filename(filename),
    )
