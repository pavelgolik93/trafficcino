from django.forms import BaseInlineFormSet


class ProductImageInlineFormSet(BaseInlineFormSet):
    def get_queryset(self):
        return super().get_queryset().exclude(product_source__isnull=False)


class AlwaysChangedBaseInlineFormSet(BaseInlineFormSet):
    def has_changed(self):
        return True
