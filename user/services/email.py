from django.conf import settings
from django.template.loader import render_to_string

import utils


def send_letter_for_account_confirmation(user_email: str, token: str) -> None:
    subject = "Trafficcino: Email confirmation"
    context = {
        "confirmation_email_url": utils.build_url(
            path=settings.EMAIL_CONFIRMATION_PATH, params={"token": token}
        ),
    }
    email_html_message = render_to_string("email/email_confirmation.html", context)
    email_plaintext_message = render_to_string("email/email_confirmation.txt", context)

    utils.send_letter(
        subject,
        to_email=user_email,
        email_plaintext_message=email_plaintext_message,
        email_html_message=email_html_message,
    )


def send_letter_for_password_reset(user_email: str, token: str) -> None:
    subject = "Trafficcino: Password reset"
    context = {
        "email": user_email,
        "reset_password_url": utils.build_url(
            path=settings.PASSWORD_RESET_PATH, params={"token": token}
        ),
    }
    email_html_message = render_to_string("email/user_reset_password.html", context)
    email_plaintext_message = render_to_string("email/user_reset_password.txt", context)

    utils.send_letter(
        subject,
        to_email=user_email,
        email_plaintext_message=email_plaintext_message,
        email_html_message=email_html_message,
    )
