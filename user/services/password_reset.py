from datetime import timedelta

from django.db import transaction
from django.utils import timezone

from . import email as email_service
from ..models import User, PasswordResetToken


def _clear_expired_password_rest_tokens():
    expiry_time = timezone.now() - timedelta(
        hours=PasswordResetToken.EXPIRATION_TIME_IN_HOURS
    )
    PasswordResetToken.objects.filter(created_at__lte=expiry_time).delete()


@transaction.atomic()
def password_reset_create_token_and_send_letter(email: str) -> None:
    _clear_expired_password_rest_tokens()

    user = User.objects.filter(email__iexact=email, is_staff=False).first()
    if user is None:
        return

    password_reset_token = user.password_reset_tokens.first()
    if password_reset_token is None:
        password_reset_token = PasswordResetToken.objects.create(user=user)
    email_service.send_letter_for_password_reset(
        user.email, password_reset_token.key.hex
    )


@transaction.atomic()
def confirm_password_reset(token: PasswordResetToken, password: str) -> None:
    token.user.set_password(password)
    token.user.save()

    # Delete all password reset tokens for this user
    PasswordResetToken.objects.filter(user=token.user).delete()
