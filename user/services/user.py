from django.db import transaction
from rest_framework.authtoken.models import Token

from . import account_confirmation
from .. import selectors
from ..models import User


def _create_user_auth_token(user_id: int) -> Token:
    return Token.objects.create(user_id=user_id)


def _delete_user_auth_token(user_id: int) -> None:
    Token.objects.filter(user_id=user_id).delete()


def user_login(email: str, password: str) -> str:
    user = selectors.user_by_email(email)
    user_token = selectors.user_auth_token(user.id)
    if user_token is None:
        user_token = _create_user_auth_token(user.id)
    return user_token.key


def user_logout(user_id: int) -> None:
    _delete_user_auth_token(user_id)


@transaction.atomic()
def user_create(email: str, password: str) -> None:
    user = User.objects.create_user(
        email=email, password=password, email_is_confirmed=False
    )
    account_confirmation.email_confirm_create_token_and_send_letter(user)
