from django.db import transaction

from . import email
from ..models import EmailConfirmationToken, User


@transaction.atomic()
def email_confirm_create_token_and_send_letter(user: User) -> None:
    user_email_confirmation_token = EmailConfirmationToken.objects.create(user=user)
    email.send_letter_for_account_confirmation(
        user_email=user.email, token=user_email_confirmation_token.key.hex
    )


@transaction.atomic()
def confirm_user_email(token: EmailConfirmationToken) -> None:
    confirm_user_account(token.user.id)
    token.delete()


def confirm_user_account(user_id: int) -> None:
    User.objects.filter(id=user_id).update(email_is_confirmed=True)
