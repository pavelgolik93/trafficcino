from typing import Union

from rest_framework.authtoken.models import Token

import utils
from .models import User, EmailConfirmationToken, PasswordResetToken


def user_auth_token(user_id: int) -> Union[Token, None]:
    return utils.get_object(Token, user_id=user_id)


def user_by_email(email: str) -> Union[User, None]:
    return User.objects.filter(email__iexact=email).first()


def email_confirmation_by_token(token: str) -> Union[EmailConfirmationToken, None]:
    return utils.get_object(EmailConfirmationToken, key=token)


def password_reset_by_token(token: str) -> Union[PasswordResetToken, None]:
    return utils.get_object(PasswordResetToken, key=token)
