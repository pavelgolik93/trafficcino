from django.urls import path

from . import views


app_name = "user_public"

urlpatterns = [
    path("create/", views.UserCreateAPI.as_view(), name="create"),
    path("login/", views.UserLoginAPI.as_view(), name="login"),
    path(
        "email/confirm/",
        views.UserAccountConfirmAPI.as_view(),
        name="user-account-confirm",
    ),
    path(
        "password/reset/",
        views.UserPasswordResetAPI.as_view(),
        name="user-password-reset",
    ),
    path(
        "password/confirm/",
        views.UserPasswordResetConfirmAPI.as_view(),
        name="user-password-reset-confirm",
    ),
]
