from django.urls import path

from . import views


app_name = "user_private"

urlpatterns = [
    path("info/", views.UserRetrieveAPI.as_view(), name="info"),
    path("info/update/", views.UserUpdateAPI.as_view(), name="info-update"),
    path("logout/", views.UserLogoutAPI.as_view(), name="logout"),
]
