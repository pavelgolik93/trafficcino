from rest_framework import serializers, fields
from rest_framework.exceptions import ValidationError

from .. import selectors
from ..models import User


class UserLoginSerializer(serializers.Serializer):
    email = serializers.EmailField()
    password = serializers.CharField()

    def validate(self, attrs):
        email = attrs["email"]
        password = attrs["password"]

        user = selectors.user_by_email(email)
        if user is None or (user and user.check_password(password) is False):
            raise ValidationError(
                {"non_field_errors": ["Unable to log in with provided credentials."]}
            )

        if user.email_is_confirmed is False:
            raise ValidationError(
                {"non_field_errors": ["You need to confirm your email."]}
            )
        return attrs


class UserCreateSerializer(serializers.Serializer):
    email = serializers.CharField()
    password = serializers.CharField()

    def validate_email(self, value):
        if selectors.user_by_email(value) is not None:
            raise ValidationError("User with this Email already exists.")
        return value


class UserUpdateSerializer(serializers.ModelSerializer):
    first_name = serializers.CharField()
    last_name = serializers.CharField()

    class Meta:
        model = User
        fields = ("id", "first_name", "last_name")


class UserRetrieveSerializer(serializers.Serializer):
    first_name = serializers.CharField()
    last_name = serializers.CharField()


class UserAccountConfirmSerializer(serializers.Serializer):
    token = fields.UUIDField()

    def validate_token(self, value):
        email_confirmation_token = selectors.email_confirmation_by_token(value)
        if email_confirmation_token is None:
            raise ValidationError(
                "Sorry, link has expired. Try to go through the procedure again."
            )
        return email_confirmation_token


class UserPasswordResetSerializer(serializers.Serializer):
    email = serializers.EmailField()


class UserPasswordResetConfirmSerializer(serializers.Serializer):
    token = serializers.CharField()
    password = serializers.CharField()

    def validate_token(self, value):
        password_reset_token = selectors.password_reset_by_token(value)
        if (
            password_reset_token is None
            or password_reset_token.token_is_expired() is True
        ):
            raise ValidationError(
                "Sorry, link has expired. Try to go through the procedure again."
            )

        return password_reset_token
