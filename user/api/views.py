from rest_framework import views, status, generics
from rest_framework.permissions import AllowAny
from rest_framework.response import Response

from . import serializers
from .. import services


class UserLoginAPI(views.APIView):
    permission_classes = (AllowAny,)
    serializer_class = serializers.UserLoginSerializer

    def post(self, request, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)

        token = services.user_login(**serializer.validated_data)
        return Response(data={"auth_token": token})


class UserLogoutAPI(views.APIView):
    def post(self, request):
        services.user_logout(request.user.id)
        return Response(status=status.HTTP_200_OK)


class UserCreateAPI(views.APIView):
    permission_classes = (AllowAny,)
    serializer_class = serializers.UserCreateSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)

        services.user_create(**serializer.validated_data)
        return Response(status=status.HTTP_201_CREATED)


class UserUpdateAPI(generics.GenericAPIView):
    serializer_class = serializers.UserUpdateSerializer

    def patch(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data, instance=request.user)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(data=serializer.data)


class UserRetrieveAPI(views.APIView):
    serializer_class = serializers.UserRetrieveSerializer

    def get(self, request, *args, **kwargs):
        serializer = self.serializer_class(instance=request.user)
        return Response(data=serializer.data)


class UserAccountConfirmAPI(views.APIView):
    permission_classes = (AllowAny,)
    serializer_class = serializers.UserAccountConfirmSerializer

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)

        services.confirm_user_email(**serializer.validated_data)
        return Response(status=status.HTTP_200_OK)


class UserPasswordResetAPI(generics.GenericAPIView):
    permission_classes = (AllowAny,)
    serializer_class = serializers.UserPasswordResetSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)

        services.password_reset_create_token_and_send_letter(
            **serializer.validated_data
        )
        return Response(status=status.HTTP_200_OK)


class UserPasswordResetConfirmAPI(generics.GenericAPIView):
    permission_classes = (AllowAny,)
    serializer_class = serializers.UserPasswordResetConfirmSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)

        services.confirm_password_reset(**serializer.validated_data)
        return Response(status=status.HTTP_200_OK)
