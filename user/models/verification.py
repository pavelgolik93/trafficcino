import uuid
from datetime import timedelta

from django.conf import settings
from django.db import models
from django.utils import timezone
from django.utils.translation import gettext_lazy as _


class Base(models.Model):
    key = models.UUIDField(default=uuid.uuid4, db_index=True, unique=True)
    created_at = models.DateTimeField(default=timezone.now)

    EXPIRATION_TIME_IN_HOURS = None

    class Meta:
        abstract = True

    def token_is_expired(self) -> bool:
        if self.EXPIRATION_TIME_IN_HOURS is None:
            raise Exception("'EXPIRATION_TIME_IN_HOURS' must be set.")

        expiry_datetime = self.created_at + timedelta(
            hours=self.EXPIRATION_TIME_IN_HOURS, seconds=-5
        )
        return timezone.now() >= expiry_datetime


class EmailConfirmationToken(Base):
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        verbose_name=_("user"),
        related_name="email_confirmation_tokens",
        on_delete=models.CASCADE,
    )

    EXPIRATION_TIME_IN_HOURS = 24 * 7


class PasswordResetToken(Base):
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        verbose_name=_("user"),
        related_name="password_reset_tokens",
        on_delete=models.CASCADE,
    )

    EXPIRATION_TIME_IN_HOURS = 24
