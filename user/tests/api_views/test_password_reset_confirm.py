from datetime import timedelta

from django.urls import reverse
from django.utils import timezone
from faker import Faker
from rest_framework import status
from rest_framework.test import APITransactionTestCase

from user.models import User, PasswordResetToken


fake = Faker()


class PasswordResetConfirmTest(APITransactionTestCase):
    fixtures = [
        "user/fixtures/user.json",
    ]
    reset_sequences = True

    def setUp(self) -> None:
        self.url = reverse("user_public:user-password-reset-confirm")

        self.user = User.objects.get(pk=1)

    def test_setting_new_password(self):
        token = PasswordResetToken.objects.create(user=self.user).key
        new_password = fake.password()

        response = self.client.post(
            self.url, data={"token": token, "password": new_password}
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.user.refresh_from_db()
        self.assertEqual(self.user.check_password(new_password), True)
        self.assertEqual(
            PasswordResetToken.objects.filter(user=self.user).exists(), False
        )

    def test_setting_new_password_with_bad_token(self):
        response = self.client.post(
            self.url, data={"token": "empty", "password": fake.password()}
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_setting_new_password_with_expired_token(self):
        token = PasswordResetToken.objects.create(
            user=self.user, created_at=timezone.now() - timedelta(days=1)
        ).key

        response = self.client.post(
            self.url, data={"token": token, "password": fake.password()}
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
