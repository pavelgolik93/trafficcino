from django.core.exceptions import ObjectDoesNotExist
from django.urls import reverse
from faker import Faker
from rest_framework import status
from rest_framework.test import APITransactionTestCase

from user.models import User, EmailConfirmationToken


fake = Faker()


class UserAccountConfirmTest(APITransactionTestCase):
    reset_sequences = True

    def setUp(self) -> None:
        self.url = reverse("user_public:user-account-confirm")

        self.user = User.objects.create(email=fake.email(), password=fake.password())

    def test_user_account_confirm(self):
        token = EmailConfirmationToken.objects.create(user=self.user).key

        response = self.client.post(self.url, data={"token": token})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(self.user.email_is_confirmed, False)
        self.user.refresh_from_db()
        self.assertEqual(self.user.email_is_confirmed, True)
        with self.assertRaises(ObjectDoesNotExist):
            EmailConfirmationToken.objects.get(user=self.user)

    def test_user_account_confirm_with_bad_token(self):
        response = self.client.post(self.url, data={"token": "empty"})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
