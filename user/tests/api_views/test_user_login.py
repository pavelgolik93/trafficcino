from faker import Faker
from rest_framework import status
from rest_framework.reverse import reverse
from rest_framework.test import APITestCase

from ...models import User


fake = Faker()


class UserLoginTest(APITestCase):
    def setUp(self) -> None:
        self.url = reverse("user_public:login")

        self.email = fake.email()
        self.password = fake.password()

        User.objects.create_user(
            email=self.email, password=self.password, email_is_confirmed=True
        )

    def test_success_login(self):
        response = self.client.post(
            self.url, data={"email": self.email, "password": self.password}
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertIn("auth_token", response.data)

    def test_login_with_not_confirmed_email(self):
        User.objects.filter(email=self.email).update(email_is_confirmed=False)

        response = self.client.post(
            self.url, data={"email": self.email, "password": self.password}
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_failed_login(self):
        response = self.client.post(
            self.url, data={"email": fake.email(), "password": self.password}
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        response = self.client.post(
            self.url, data={"email": self.email, "password": fake.password()}
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
