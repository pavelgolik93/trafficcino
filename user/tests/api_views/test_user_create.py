from unittest.mock import patch

from django.urls import reverse
from faker import Faker
from rest_framework import status
from rest_framework.test import APITransactionTestCase

from user.models import User, EmailConfirmationToken


fake = Faker()


class UserCreateTest(APITransactionTestCase):
    reset_sequences = True

    def setUp(self) -> None:
        self.url = reverse("user_public:create")

    @patch("utils.email.send_letter")
    def test_create(self, *args, **kwargs):
        email = fake.email()
        password = fake.password()

        response = self.client.post(
            self.url, data={"email": email, "password": password}
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        new_user = User.objects.get(pk=1)
        self.assertEqual(new_user.email, email)
        self.assertEqual(new_user.email_is_confirmed, False)
        self.assertEqual(
            EmailConfirmationToken.objects.filter(user=new_user).exists(), True
        )

    def test_create_with_not_exist_email(self):
        email = fake.email()
        password = fake.password()

        User.objects.create(email=email, password=password)

        response = self.client.post(
            self.url, data={"email": email, "password": password}
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
