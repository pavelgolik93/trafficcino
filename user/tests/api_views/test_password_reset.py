from unittest.mock import patch

from django.urls import reverse
from faker import Faker
from rest_framework import status
from rest_framework.test import APITransactionTestCase

from user.models import User, PasswordResetToken


fake = Faker()


class PasswordResetTest(APITransactionTestCase):
    reset_sequences = True

    def setUp(self) -> None:
        self.url = reverse("user_public:user-password-reset")

    @patch("utils.email.send_letter")
    def test_reset_password_request(self, *args, **kwargs):
        email = fake.email()
        user = User.objects.create(email=email, password=fake.password())

        response = self.client.post(self.url, data={"email": email})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(PasswordResetToken.objects.filter(user=user).exists(), True)

    @patch("utils.email.send_letter")
    def test_reset_password_request_with_bad_email(self, *args, **kwargs):
        email = fake.email()
        response = self.client.post(self.url, data={"email": email})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            PasswordResetToken.objects.filter(user__email=email).exists(), False
        )
