from django.apps import AppConfig


class StripeChargeConfig(AppConfig):
    name = "stripe_charge"
    verbose_name = "Stripe charge"
