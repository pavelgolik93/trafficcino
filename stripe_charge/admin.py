from django.contrib import admin

from . import models


@admin.register(models.TariffPlan)
class TariffPlanAdmin(admin.ModelAdmin):
    pass


@admin.register(models.ChargeProfile)
class ChargeProfileAdmin(admin.ModelAdmin):
    list_display = ("user", "customer_id", "created_at")
    readonly_fields = ("created_at",)


@admin.register(models.Subscription)
class SubscriptionAdmin(admin.ModelAdmin):
    list_display = (
        "charge_profile",
        "subscription_id",
        "tariff_plan",
        "latest_invoice_id",
        "status",
    )
    readonly_fields = ("created_at",)
