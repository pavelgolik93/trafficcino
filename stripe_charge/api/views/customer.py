from rest_framework.decorators import api_view
from rest_framework.response import Response

from ... import services


@api_view()
def create_checkout_session(request):
    plan_id = request.query_params["plan_id"]
    base_url = request.build_absolute_uri("/")
    customer_id = request.user.charge_profile.customer_id
    return Response(
        {"url": services.get_checkout_session_url(plan_id, customer_id, base_url)}
    )


@api_view()
def create_billing_portal(request):
    base_url = request.build_absolute_uri("/")
    customer_id = request.user.charge_profile.customer_id
    return Response({"url": services.get_billing_portal_url(customer_id, base_url)})
