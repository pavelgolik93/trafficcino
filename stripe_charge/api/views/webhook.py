from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from app.permissions import AllowOnlyStripeWebhooks
from ... import services


class WebhookHandler(APIView):
    permission_classes = (AllowOnlyStripeWebhooks,)

    @property
    def payload(self):
        return self.request.data["data"]

    def startup_action(self, event_type):
        match event_type:
            case "invoice.paid":
                self.invoice_paid()
            case "invoice.payment_failed":
                self.invoice_payment_failed()
            case "payment_intent.payment_failed":
                self.payment_indent_failed()
            case "invoice.upcoming":
                self.invoice_upcoming()
            case "customer.subscription.updated":
                self.customer_subscription_updated()
            case "customer.subscription.deleted":
                self.customer_subscription_deleted()

    def post(self, request):
        self.startup_action(event_type=request.data["type"])
        return Response(status=status.HTTP_200_OK)

    def invoice_paid(self):
        data_object = self.payload["object"]
        billing_reason = data_object["billing_reason"]
        subscription_data = data_object["lines"]["data"][0]

        match billing_reason:
            case "subscription_create":
                services.create_subscriptions(
                    data_object["customer"], data_object["id"], subscription_data
                )
            case "subscription_cycle":
                services.subscription_new_cycle(subscription_data)

    def invoice_upcoming(self):
        """
        Происходит за несколько дней до продления подписки
        """

    def invoice_payment_failed(self):
        """
        Происходит если с картой какие-то проблемы
        """
        # TODO отправить пользователю эмейл чтоб обновил способы оплаты

    def payment_indent_failed(self):
        """
        Происходит если требуется 3DS авторизация
        """
        # TODO отправить email что бы пользователь подтвердил оплату

    def customer_subscription_updated(self):
        services.subscription_update(
            self.payload["object"], self.payload["previous_attributes"]
        )

    def customer_subscription_deleted(self):
        services.subscription_delete(self.payload["object"]["id"])
