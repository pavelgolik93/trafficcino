from django.urls import path

from .views.webhook import WebhookHandler

app_name = "stripe_charge_public"

urlpatterns = [
    path("webhooks/", WebhookHandler.as_view(), name="webhook-handler"),
]
