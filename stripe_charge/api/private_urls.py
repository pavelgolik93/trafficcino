from django.urls import path

from .views import customer


app_name = "stripe_charge_private"

urlpatterns = [
    path(
        "checkout-session/",
        customer.create_checkout_session,
        name="create-checkout-session",
    ),
    path(
        "billing-portal/", customer.create_billing_portal, name="create-billing-portal"
    ),
]
