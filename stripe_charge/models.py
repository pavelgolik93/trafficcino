from django.conf import settings
from django.db import models
from django.utils import timezone
from django.utils.translation import gettext_lazy as _


class TariffPlan(models.Model):
    name = models.CharField(_("name"), max_length=50)
    price = models.DecimalField(_("price"), max_digits=7, decimal_places=2)
    price_id = models.CharField(
        _("price id"), max_length=50, help_text=_("Price id from Stripe product.")
    )
    landings_amount = models.IntegerField(_("landings amount"), blank=True, null=True)
    basic_analytics = models.BooleanField(_("basic analytics"), default=False)
    channel_analytics = models.BooleanField(_("channel analytics"), default=False)
    basic_custom_design = models.BooleanField(_("basic custom design"), default=False)
    email_phone_collection = models.BooleanField(
        _("email/phone collection"), default=False
    )
    customer_analytics = models.BooleanField(_("customer analytics"), default=False)
    sales_analytics = models.BooleanField(_("sales analytics"), default=False)
    advanced_custom_design = models.BooleanField(
        _("advanced custom design"), default=False
    )
    managed_service = models.BooleanField(_("managed service"), default=False)

    # Private settings
    archived = models.BooleanField(default=False)

    def __str__(self):
        return "{} ${} USD".format(self.name, self.price)


class ChargeProfile(models.Model):
    user = models.OneToOneField(
        settings.AUTH_USER_MODEL,
        verbose_name=_("user"),
        related_name="charge_profile",
        primary_key=True,
        on_delete=models.CASCADE,
    )
    customer_id = models.CharField(_("customer id"), max_length=40, unique=True)
    created_at = models.DateTimeField(_("created at"), default=timezone.now)

    def __str__(self):
        return str(self.user)


class Subscription(models.Model):
    class Status(models.TextChoices):
        ACTIVE = ("active", "Active")
        UNPAID = ("unpaid", "Unpaid")

    charge_profile = models.OneToOneField(
        "stripe_charge.ChargeProfile",
        verbose_name=_("charge profile"),
        primary_key=True,
        on_delete=models.CASCADE,
    )
    subscription_id = models.CharField(_("subscription id"), max_length=40, unique=True)
    tariff_plan = models.ForeignKey(
        "stripe_charge.TariffPlan", verbose_name=_("tariff"), on_delete=models.PROTECT
    )
    period_start = models.DateTimeField(_("period start"))
    period_end = models.DateTimeField(_("period end"))
    latest_invoice_id = models.CharField(
        _("latest invoice id"), max_length=40, unique=True
    )
    status = models.CharField(
        max_length=10, choices=Status.choices, default=Status.ACTIVE
    )
    updated_at = models.DateTimeField(_("updated at"), auto_now=True)
    created_at = models.DateTimeField(_("created at"), default=timezone.now)
