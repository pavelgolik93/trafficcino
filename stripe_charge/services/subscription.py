from landing import services as landing_services
from stripe_charge import utils
from ..models import ChargeProfile, TariffPlan, Subscription


# оформление подписки
# https://stripe.com/docs/billing/subscriptions/overview#subscription-payment-behavior
# для создания интерфейса на фронте
# https://stripe.com/docs/billing/subscriptions/build-subscriptions?ui=elements


def create_subscriptions(
    customer_id: str, invoice_id: str, subscription_data: dict
) -> None:
    assert subscription_data["type"] == "subscription"
    assert subscription_data["subscription"] is not None

    charge_profile = ChargeProfile.objects.get(customer_id=customer_id)
    price_id = subscription_data["price"]["id"]
    tariff_plan = TariffPlan.objects.get(price_id=price_id)

    subscription = Subscription.objects.create(
        charge_profile=charge_profile,
        tariff_plan=tariff_plan,
        subscription_id=subscription_data["subscription"],
        latest_invoice_id=invoice_id,
        period_start=utils.timestamp_to_datetime(subscription_data["period"]["start"]),
        period_end=utils.timestamp_to_datetime(subscription_data["period"]["end"]),
    )

    landing_services.check_active_landings_in_relation_to_tariff_plan_limit(
        subscription.charge_profile.user_id, subscription.tariff_plan.landings_amount
    )


def subscription_update(new_subscription_data: dict, previous_subscription_data: dict):
    subscription = Subscription.objects.get(subscription_id=new_subscription_data["id"])

    if "current_period_start" in previous_subscription_data:
        subscription.period_start = utils.timestamp_to_datetime(
            new_subscription_data["current_period_start"]
        )

    if "current_period_end" in previous_subscription_data:
        subscription.period_end = utils.timestamp_to_datetime(
            new_subscription_data["current_period_end"]
        )

    new_status = new_subscription_data["status"]
    if "status" in previous_subscription_data:
        assert new_status in Subscription.Status

        if (
            subscription.status == Subscription.Status.ACTIVE
            and new_status in Subscription.Status.UNPAID
        ):
            landing_services.set_disable_status_for_all_user_landing_pools(
                subscription.charge_profile.user_id
            )
        subscription.status = new_status

    if "latest_invoice" in previous_subscription_data:
        subscription.latest_invoice_id = new_subscription_data["latest_invoice"]

    new_plan_id = new_subscription_data["plan"]["id"]
    if "plan" in previous_subscription_data and new_plan_id != subscription.tariff_plan:
        subscription.tariff_plan = TariffPlan.objects.get(price_id=new_plan_id)
        landing_services.check_active_landings_in_relation_to_tariff_plan_limit(
            subscription.charge_profile.user_id,
            subscription.tariff_plan.landings_amount,
        )

    subscription.save()


def subscription_delete(subscription_id: str):
    try:
        subscription = Subscription.objects.get(subscription_id=subscription_id)
    except Subscription.DoesNotExist:
        # TODO отослать варнинг в сентри
        pass
    else:
        landing_services.set_disable_status_for_all_user_landing_pools(
            subscription.charge_profile.user_id
        )
        subscription.delete()


def subscription_new_cycle(subscription_data: dict):
    assert subscription_data["type"] == "subscription"
    assert subscription_data["subscription"] is not None

    subscription = Subscription.objects.get(
        subscription_id=subscription_data["subscription"]
    )
    subscription.period_start = utils.timestamp_to_datetime(
        subscription_data["period"]["start"]
    )
    subscription.period_end = utils.timestamp_to_datetime(
        subscription_data["period"]["end"]
    )
    subscription.status = Subscription.Status.ACTIVE
    subscription.save()

    landing_services.check_active_landings_in_relation_to_tariff_plan_limit(
        subscription.charge_profile.user_id, subscription.tariff_plan.landings_amount
    )
