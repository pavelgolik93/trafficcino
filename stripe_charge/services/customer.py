from urllib.parse import urljoin

import stripe
from django.conf import settings

from app.decorators import stripe_init_session_decorator
from ..models import TariffPlan, ChargeProfile


@stripe_init_session_decorator
def get_checkout_session_url(
    plan_id: int, stripe_customer_id: str, base_url: str
) -> str:
    tariff = TariffPlan.objects.get(pk=plan_id)

    checkout_session = stripe.checkout.Session.create(
        mode="subscription",
        customer=stripe_customer_id,
        payment_method_types=["card"],
        line_items=[
            {"price": tariff.price_id, "quantity": 1},
        ],
        success_url=urljoin(base_url, settings.CHECKOUT_SESSION_SUCCESS_PATH),
        cancel_url=urljoin(base_url, settings.CHECKOUT_SESSION_CANCEL_PATH),
    )
    return checkout_session["url"]


@stripe_init_session_decorator
def get_billing_portal_url(stripe_customer_id: str, base_url: str) -> str:
    portal_session = stripe.billing_portal.Session.create(
        customer=stripe_customer_id,
        return_url=urljoin(base_url, settings.BILLING_PORTAL_RETURN_PATH),
    )
    return portal_session["url"]


@stripe_init_session_decorator
def create_and_linked_stripe_customer_profile(user_id: int, email: str) -> None:
    query = "email:'{}'".format(email)
    customer_search_result = stripe.Customer.search(query=query)
    if customer_search_result["data"]:
        customer = customer_search_result["data"][0]
    else:
        customer = stripe.Customer.create(email=email)
    ChargeProfile.objects.create(user_id=user_id, customer_id=customer["id"])
