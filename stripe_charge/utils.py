from datetime import datetime

from django.utils import timezone


def timestamp_to_datetime(timestamp: int):
    return datetime.fromtimestamp(timestamp, tz=timezone.get_default_timezone())
