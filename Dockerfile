FROM --platform=linux/amd64 python:3.10.5-slim


RUN apt-get -y update \
    && apt-get -y install gcc wget libmaxminddb0 libmaxminddb-dev mmdb-bin binutils libproj-dev gdal-bin

WORKDIR /app
COPY . /app

RUN pip install --upgrade pip poetry \
    && poetry config virtualenvs.create false \
    && poetry install --no-interaction --no-ansi --no-dev

RUN chmod a+x /app/load_fixtures.sh

ENV PYTHONPATH /app
ENV PYTHONUNBUFFERED 1

EXPOSE 80
