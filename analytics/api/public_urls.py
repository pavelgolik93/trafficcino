from django.urls import path

from . import views


app_name = "analytics_public"

urlpatterns = [
    path(
        "landing/<uuid:landing_id>/event/",
        views.LandingEventCreateAPI.as_view(),
        name="track-landing-event",
    ),
    path(
        "landing/<uuid:landing_id>/collect/contacts/",
        views.CoollectPersonalDataAPI.as_view(),
        name="coollect-personal-data",
    ),
]
