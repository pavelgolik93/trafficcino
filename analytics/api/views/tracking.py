from rest_framework import views, generics, status
from rest_framework.permissions import AllowAny
from rest_framework.response import Response

from .. import serializers


class LandingEventCreateAPI(views.APIView):
    permission_classes = (AllowAny,)
    serializer_class = serializers.CollectedDataSerializer

    def post(self, request, landing_id, *args, **kwargs):
        serializer = self.serializer_class(
            data={**request.data, "landing": landing_id}, context={"request": request}
        )
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(data=serializer.data, status=status.HTTP_201_CREATED)


class CoollectPersonalDataAPI(generics.GenericAPIView):
    permission_classes = (AllowAny,)
    serializer_class = serializers.ClientPersonalDataSerializer

    def post(self, request, landing_id):
        serializer = self.get_serializer(data={**request.data, "landing": landing_id})
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(status=status.HTTP_201_CREATED)
