from rest_framework.response import Response
from rest_framework import views

from ... import services


class Insights(views.APIView):
    @property
    def landing_pool_id(self):
        return self.request.query_params.get("landing_pool_id")

    @property
    def date_range(self):
        start_date = self.request.query_params.get("start_date")
        end_date = self.request.query_params.get("end_date")

        if start_date and end_date:
            return [start_date, end_date]
        return None

    def get(self, request, *args, **kwargs):
        insights = services.Insights(
            user_id=request.user.id,
            landing_pool_id=self.landing_pool_id,
            date_range=self.date_range,
        )
        return Response(insights.get())
