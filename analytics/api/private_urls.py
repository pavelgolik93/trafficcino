from django.urls import path

from . import views


app_name = "analytics_private"

urlpatterns = [
    path("insights/", views.Insights.as_view()),
]
