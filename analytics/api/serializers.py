from typing import Union
from uuid import uuid4

import user_agents
from django.conf import settings
from django.contrib.gis.geoip2 import GeoIP2
from django.core.validators import EmailValidator
from django.utils.functional import cached_property
from geoip2.errors import AddressNotFoundError
from rest_framework import serializers, fields
from django.core.exceptions import ValidationError as DjangoValidationError
from ua_parser import user_agent_parser

from landing.models import ProductSource, Landing
from ..exceptions import ValidationError
from ..models import LandingEvent, ClientPersonalData

VISITOR_NOT_SET = 1201
DATA_BY_VISITOR_NOT_FOUND = 1202
SESSION_NOT_SET = 1203
START_EVENT_NOT_FOUND = 1204
PRODUCT_SOURCE_ID_NOT_SET = 1205
PRODUCT_NOT_FOUND = 1206
PRODUCT_DOES_NOT_BELONG_LANDING_PAGE = 1207
EMAIL_AND_PHONE_EMPTY = 1208


class UserAgentDefault(serializers.CurrentUserDefault):
    def __call__(self, serializer_field):
        user_agent_string = serializer_field.context["request"].META.get(
            settings.DJANGO_REST_PASSWORDRESET_HTTP_USER_AGENT_HEADER, None
        )

        parser = user_agents.parse(user_agent_string)
        return {
            **user_agent_parser.Parse(user_agent_string),
            "is_pc": parser.is_pc,
            "is_mobile": parser.is_mobile,
            "is_tablet": parser.is_tablet,
        }


class IpAddressDefault(serializers.CurrentUserDefault):
    def __call__(self, serializer_field):
        ip = serializer_field.context["request"].META.get(
            settings.DJANGO_REST_PASSWORDRESET_IP_ADDRESS_HEADER, None
        )
        try:
            city_info = GeoIP2().city(ip)
        except AddressNotFoundError:
            city_info = {}
        return {"ip": ip, **city_info}


class VisitorDefault(serializers.CurrentUserDefault):
    def __call__(self, serializer_field):
        return serializer_field.context["request"].COOKIES.get("visitor", None)


class SessionDefault(serializers.CurrentUserDefault):
    def __call__(self, serializer_field):
        return serializer_field.context["request"].headers.get("X-SESSION", None)


class CollectedDataSerializer(serializers.ModelSerializer):
    event_type = fields.ChoiceField(choices=LandingEvent.Types.choices)
    visitor = serializers.HiddenField(default=VisitorDefault())
    session = serializers.HiddenField(default=SessionDefault())
    user_agent = serializers.HiddenField(default=dict, required=False)
    ip_address = serializers.HiddenField(default=dict, required=False)
    product_source = serializers.PrimaryKeyRelatedField(
        queryset=ProductSource.objects.all(),
        default=None,
        required=False,
        allow_null=True,
    )

    class Meta:
        model = LandingEvent
        fields = (
            "landing",
            "event_type",
            "visitor",
            "session",
            "user_agent",
            "ip_address",
            "product_source",
        )

    @cached_property
    def current_event_type(self):
        return self.get_initial()["event_type"]

    @property
    def is_event_type_new_visit(self):
        return self.current_event_type == LandingEvent.Types.NEW_VISIT

    @property
    def is_event_type_transition_to(self):
        return self.current_event_type == LandingEvent.Types.TRANSITION_TO

    @property
    def creds_must_be_set(self):
        return self.current_event_type in (
            LandingEvent.Types.TIME_TRACKING,
            LandingEvent.Types.TRANSITION_TO,
        )

    def validate_visitor(self, value):
        if self.creds_must_be_set and value is None:
            raise ValidationError(VISITOR_NOT_SET)

        if value and not LandingEvent.objects.filter(visitor=value).exists():
            if self.creds_must_be_set:
                raise ValidationError(DATA_BY_VISITOR_NOT_FOUND)

            value = None

        return uuid4().hex if value is None else value

    def validate_session(self, value):
        visitor = VisitorDefault()(self)

        if self.creds_must_be_set:
            if value is None:
                raise ValidationError(SESSION_NOT_SET)

            main_event = LandingEvent.objects.filter(
                visitor=visitor,
                session=value,
                event_type=LandingEvent.Types.NEW_VISIT,
            )
            if not main_event.exists():
                raise ValidationError(START_EVENT_NOT_FOUND)

        if self.is_event_type_new_visit:
            value = uuid4().hex

        return value

    def validate_product_source(self, value: Union[ProductSource, None]):
        landing_id = self.get_initial()["landing"].hex

        if self.is_event_type_transition_to:
            if value is None:
                raise ValidationError(PRODUCT_SOURCE_ID_NOT_SET)

            landing = Landing.objects.get(id=landing_id)
            if value.product.landing_pool_id != landing.landing_pool_id:
                raise ValidationError(PRODUCT_DOES_NOT_BELONG_LANDING_PAGE)

        return value

    def to_internal_value(self, data):
        ret = super().to_internal_value(data)

        if self.is_event_type_new_visit:
            ret.update(
                {
                    "user_agent": UserAgentDefault()(self),
                    "ip_address": IpAddressDefault()(self),
                }
            )
        return ret

    def to_representation(self, instance):
        if self.is_event_type_new_visit:
            return {"visitor": instance.visitor, "session": instance.session}
        return {}


class ClientPersonalDataSerializer(serializers.ModelSerializer):
    visitor = serializers.HiddenField(default=VisitorDefault())
    session = serializers.HiddenField(default=SessionDefault())
    email = fields.CharField(required=False, allow_blank=True, max_length=50)
    phone = fields.CharField(required=False, allow_blank=True, max_length=20)

    class Meta:
        model = ClientPersonalData
        fields = (
            "visitor",
            "session",
            "landing",
            "phone",
            "email",
        )

    def validate_email(self, email: str):
        email_validator = EmailValidator()
        try:
            email_validator(email)
        except DjangoValidationError:
            return ""
        return email

    def validate(self, attrs):
        main_event = LandingEvent.objects.filter(
            event_type=LandingEvent.Types.NEW_VISIT,
            visitor=attrs["visitor"],
            session=attrs["session"],
        )
        if not main_event.exists():
            raise ValidationError(START_EVENT_NOT_FOUND)

        if not attrs.get("email") and not attrs.get("phone"):
            raise ValidationError(EMAIL_AND_PHONE_EMPTY)

        return attrs
