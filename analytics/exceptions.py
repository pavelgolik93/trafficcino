from rest_framework import status
from rest_framework.exceptions import APIException, _get_error_details


class ValidationError(APIException):
    status_code = status.HTTP_400_BAD_REQUEST

    def __init__(self, error_code):
        self.detail = _get_error_details(
            {"error": 1, "error_code": error_code}, error_code
        )
