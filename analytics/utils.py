import datetime

from django.utils import timezone


def atc_rate_calculation(data: list):
    return [
        {
            **row,
            "atc_rate": round((row["add_to_carts"] / row["page_views"]) * 100, 2),
        }
        for row in data
    ]


def slicing_date_range_to_timestamp_sub_range(
    star_date: datetime.datetime, end_date: datetime.datetime, ranges_count=20
):
    star_date = star_date.replace(tzinfo=timezone.utc)
    end_date = end_date.replace(
        hour=23, minute=59, second=59, microsecond=999999, tzinfo=timezone.utc
    )
    constant = (end_date.timestamp() - star_date.timestamp()) / ranges_count

    start = star_date.timestamp()
    end = star_date.timestamp() + constant
    ranges = []
    for i in range(ranges_count):
        ranges.append([start, end])

        start = end
        end += constant
    return ranges
