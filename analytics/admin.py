from django.contrib import admin

from .models import LandingEvent, ClientPersonalData


@admin.register(LandingEvent)
class LandingEventAdmin(admin.ModelAdmin):
    list_display = (
        "session",
        "visitor",
        "event_type",
        "landing",
        "created_at",
    )
    search_fields = ("visitor", "session")
    list_filter = ("event_type",)
    readonly_fields = ("created_at",)

    def _get_fieldset(self, fieldset):
        return [(None, {"fields": fieldset})]

    def get_fieldsets(self, request, obj=None):
        fieldset = ["event_type", "session", "visitor", "landing"]
        if obj:
            if obj.event_type == LandingEvent.Types.NEW_VISIT:
                fieldset.extend(["user_agent", "ip_address"])
            if obj.event_type == LandingEvent.Types.TRANSITION_TO:
                fieldset.extend(["product_source"])

            return self._get_fieldset(fieldset + ["created_at"])
        return self._get_fieldset(self.get_fields(request, obj))

    def has_add_permission(self, request):
        return False

    def has_change_permission(self, request, obj=None):
        return False


@admin.register(ClientPersonalData)
class ClientPersonalDataAdmin(admin.ModelAdmin):
    list_display = (
        "session",
        "visitor",
        "landing",
        "email",
        "phone",
        "created_at",
    )

    def has_add_permission(self, request):
        return False

    def has_change_permission(self, request, obj=None):
        return False
