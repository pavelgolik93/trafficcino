from django.db import models
from django.utils import timezone
from django.utils.translation import gettext_lazy as _


class LandingEvent(models.Model):
    class Types(models.TextChoices):
        NEW_VISIT = "new_visit", _("New visit")
        TRANSITION_TO = "transition_to", _("Transition to")
        TIME_TRACKING = "time_tracking", _("Time tracking")

    event_type = models.CharField(choices=Types.choices, max_length=20)
    visitor = models.CharField(_("visitor"), max_length=32)
    session = models.CharField(_("session"), max_length=32)
    landing = models.ForeignKey(
        "landing.Landing", verbose_name=_("landing"), on_delete=models.CASCADE
    )
    created_at = models.DateTimeField(_("created at"), default=timezone.now)

    # Event new_visit
    user_agent = models.JSONField(_("user-agent"), blank=True, default=dict)
    ip_address = models.JSONField(_("ip address"), blank=True, default=dict)

    # Event transition_to
    product_source = models.ForeignKey(
        "landing.ProductSource",
        verbose_name=_("product source"),
        on_delete=models.CASCADE,
        blank=True,
        null=True,
    )

    class Meta:
        verbose_name = _("Landing event")
        verbose_name_plural = _("Landing events")
        indexes = [
            models.Index(fields=["session", "event_type"]),
            models.Index(fields=["visitor", "session", "event_type"]),
        ]


class ClientPersonalData(models.Model):
    visitor = models.CharField(_("visitor"), max_length=32)
    session = models.CharField(_("session"), max_length=32)
    landing = models.ForeignKey(
        "landing.Landing", verbose_name=_("landing"), on_delete=models.CASCADE
    )
    email = models.CharField(max_length=50)
    phone = models.CharField(max_length=20)
    created_at = models.DateTimeField(_("created at"), default=timezone.now)

    class Meta:
        verbose_name = _("Client personal data")
        verbose_name_plural = _("Clients personal data")
