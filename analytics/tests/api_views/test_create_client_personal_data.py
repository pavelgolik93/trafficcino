from uuid import uuid4

from django.core.exceptions import ObjectDoesNotExist
from django.urls import reverse
from faker import Faker
from rest_framework import status
from rest_framework.test import APITransactionTestCase

from ...api.serializers import START_EVENT_NOT_FOUND, EMAIL_AND_PHONE_EMPTY
from ...models import ClientPersonalData, LandingEvent

fake = Faker(locale="en_US")


class TestLandingEventCreateAPI(APITransactionTestCase):
    fixtures = [
        "user/fixtures/user.json",
        "landing/fixtures/marketplace.json",
        "landing/fixtures/ad_network.json",
        "landing/fixtures/landing_pool.json",
        "landing/fixtures/product.json",
        "landing/fixtures/product_source.json",
        "landing/fixtures/landing.json",
    ]
    reset_sequences = True

    def setUp(self) -> None:
        self.visitor = uuid4().hex
        self.session = uuid4().hex
        landing_id = "22dc2f67-0a50-4e1e-a73e-bdac0f0b7be7"
        LandingEvent.objects.create(
            visitor=self.visitor,
            session=self.session,
            landing_id=landing_id,
            event_type=LandingEvent.Types.NEW_VISIT,
        )

        self.url = reverse(
            "analytics_public:coollect-personal-data", args=(landing_id,)
        )
        self.valid_headers = {
            "HTTP_COOKIE": "visitor={}".format(self.visitor),
            "HTTP_X-SESSION": self.session,
        }

    def test_create(self):
        email = fake.email()
        phone = "+1-216-220-9460"

        response = self.client.post(
            self.url, {"email": email, "phone": phone}, **self.valid_headers
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        client_personal_data = ClientPersonalData.objects.get(pk=1)
        self.assertEqual(client_personal_data.email, email)
        self.assertEqual(client_personal_data.phone, phone)

    def test_create_without_email(self):
        phone = "+1-216-220-9460"

        response = self.client.post(self.url, {"phone": phone}, **self.valid_headers)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        client_personal_data = ClientPersonalData.objects.get(pk=1)
        self.assertEqual(client_personal_data.email, "")
        self.assertEqual(client_personal_data.phone, phone)

    def test_create_without_phone(self):
        email = fake.email()

        response = self.client.post(self.url, {"email": email}, **self.valid_headers)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        client_personal_data = ClientPersonalData.objects.get(pk=1)
        self.assertEqual(client_personal_data.email, email)
        self.assertEqual(client_personal_data.phone, "")

    def test_create_with_bad_email(self):
        email = "test"
        phone = "+1-216-220-9460"

        response = self.client.post(
            self.url, {"email": email, "phone": phone}, **self.valid_headers
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        client_personal_data = ClientPersonalData.objects.get(pk=1)
        self.assertEqual(client_personal_data.email, "")
        self.assertEqual(client_personal_data.phone, phone)

    def test_create_with_empty_email_and_phone(self):
        response = self.client.post(
            self.url, {"email": "", "phone": ""}, **self.valid_headers
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data["error_code"].code, EMAIL_AND_PHONE_EMPTY)

    def test_create_with_bad_headers(self):
        email = fake.email()
        phone = "+1-216-220-9460"

        invalid_headers = {
            "HTTP_COOKIE": "visitor={}".format("test"),
            "HTTP_X-SESSION": self.session,
        }
        response = self.client.post(
            self.url, {"email": email, "phone": phone}, **invalid_headers
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data["error_code"].code, START_EVENT_NOT_FOUND)
        with self.assertRaises(ObjectDoesNotExist):
            ClientPersonalData.objects.get(pk=1)

    def test_create_without_headers(self):
        email = fake.email()
        phone = "+1-216-220-9460"

        response = self.client.post(self.url, {"email": email, "phone": phone})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data["error_code"].code, START_EVENT_NOT_FOUND)
        with self.assertRaises(ObjectDoesNotExist):
            ClientPersonalData.objects.get(pk=1)

    def test_create_with_bad_landing_id(self):
        email = fake.email()
        phone = "+1-216-220-9460"

        response = self.client.post(
            reverse(
                "analytics_public:coollect-personal-data",
                args=("22dc2f6c-0a50-4e1e-a73e-bdac0f0b7be7",),
            ),
            {"email": email, "phone": phone},
            **self.valid_headers
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        with self.assertRaises(ObjectDoesNotExist):
            ClientPersonalData.objects.get(pk=1)
