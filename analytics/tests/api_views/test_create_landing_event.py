from uuid import uuid4

from django.urls import reverse
from rest_framework import status
from rest_framework.exceptions import ErrorDetail
from rest_framework.test import APITransactionTestCase

from analytics.api.serializers import (
    SESSION_NOT_SET,
    VISITOR_NOT_SET,
    DATA_BY_VISITOR_NOT_FOUND,
    START_EVENT_NOT_FOUND,
    PRODUCT_SOURCE_ID_NOT_SET,
)
from analytics.models import LandingEvent


class TestLandingEventCreateAPI(APITransactionTestCase):
    fixtures = [
        "user/fixtures/user.json",
        "landing/fixtures/marketplace.json",
        "landing/fixtures/ad_network.json",
        "landing/fixtures/landing_pool.json",
        "landing/fixtures/product.json",
        "landing/fixtures/product_source.json",
        "landing/fixtures/landing.json",
    ]
    reset_sequences = True

    def setUp(self):
        self.visitor = uuid4().hex
        self.session = uuid4().hex
        landing_id = "22dc2f67-0a50-4e1e-a73e-bdac0f0b7be7"
        LandingEvent.objects.create(
            visitor=self.visitor,
            session=self.session,
            landing_id=landing_id,
            event_type=LandingEvent.Types.NEW_VISIT,
        )

        self.url = reverse("analytics_public:track-landing-event", args=(landing_id,))

    def _test_event_without_visitor(self, data):
        headers = {"HTTP_X-SESSION": self.session}

        response = self.client.post(self.url, data, **headers)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data["error_code"].code, VISITOR_NOT_SET)

    def _test_event_without_session(self, data):
        headers = {"HTTP_COOKIE": "visitor={}".format(self.visitor)}

        response = self.client.post(self.url, data, **headers)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data["error_code"].code, SESSION_NOT_SET)

    def _test_event_wit_wrong_visitor(self, data):
        headers = {
            "HTTP_COOKIE": "visitor={}".format(111),
            "HTTP_X-SESSION": self.session,
        }

        response = self.client.post(self.url, data, **headers)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data["error_code"].code, DATA_BY_VISITOR_NOT_FOUND)

    def _test_event_wit_wrong_session(self, data):
        headers = {
            "HTTP_COOKIE": "visitor={}".format(self.visitor),
            "HTTP_X-SESSION": "1111",
        }

        response = self.client.post(self.url, data, **headers)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data["error_code"].code, START_EVENT_NOT_FOUND)

    def test_creating_event_new_visit(self):
        data = {"event_type": LandingEvent.Types.NEW_VISIT}
        headers = {
            "HTTP_X_REAL_IP": "192.168.0.1",
            "HTTP_USER_AGENT": "My User Agent 1.0",
        }

        response = self.client.post(self.url, data, **headers)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertNotEqual(response.data["visitor"], self.visitor)

        event_from_db = LandingEvent.objects.get(id=2)
        self.assertEqual(response.data["visitor"], event_from_db.visitor)
        self.assertEqual(headers["HTTP_X_REAL_IP"], event_from_db.ip_address["ip"])
        self.assertEqual(headers["HTTP_USER_AGENT"], event_from_db.user_agent["string"])

    def test_creating_event_new_visit_with_creds(self):
        data = {"event_type": LandingEvent.Types.NEW_VISIT}
        headers = {
            "HTTP_X_REAL_IP": "192.168.0.1",
            "HTTP_USER_AGENT": "My User Agent 1.0",
            "HTTP_COOKIE": "visitor={}".format(self.visitor),
            "HTTP_X-SESSION": self.session,
        }

        response = self.client.post(self.url, data, **headers)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data["visitor"], self.visitor)
        self.assertNotEqual(response.data["session"], self.session)

        event_from_db = LandingEvent.objects.get(
            session=response.data["session"],
            event_type=LandingEvent.Types.NEW_VISIT,
        )
        self.assertEqual(headers["HTTP_X_REAL_IP"], event_from_db.ip_address["ip"])
        self.assertEqual(headers["HTTP_USER_AGENT"], event_from_db.user_agent["string"])

    def test_creating_event_new_visit_with_bad_visitor(self):
        visitor = "1111"
        data = {"event_type": LandingEvent.Types.NEW_VISIT}
        headers = {
            "HTTP_X_REAL_IP": "192.168.0.1",
            "HTTP_USER_AGENT": "My User Agent 1.0",
            "HTTP_COOKIE": "visitor={}".format(visitor),
        }

        response = self.client.post(self.url, data, **headers)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertNotEqual(response.data["visitor"], visitor)

    def test_creating_event_time_tracking(self):
        data = {"event_type": LandingEvent.Types.TIME_TRACKING}
        headers = {
            "HTTP_COOKIE": "visitor={}".format(self.visitor),
            "HTTP_X-SESSION": self.session,
        }

        response = self.client.post(self.url, data, **headers)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        event_time_tracking = LandingEvent.objects.get(
            visitor=self.visitor, event_type=LandingEvent.Types.TIME_TRACKING
        )
        self.assertEqual(event_time_tracking.visitor, self.visitor)
        self.assertEqual(event_time_tracking.session, self.session)

    def test_creating_event_transition_to(self):
        data = {
            "event_type": LandingEvent.Types.TRANSITION_TO,
            "product_source": 10,
        }
        headers = {
            "HTTP_COOKIE": "visitor={}".format(self.visitor),
            "HTTP_X-SESSION": self.session,
        }

        response = self.client.post(self.url, data, **headers)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        event_time_tracking = LandingEvent.objects.get(
            visitor=self.visitor, event_type=LandingEvent.Types.TRANSITION_TO
        )
        self.assertEqual(event_time_tracking.visitor, self.visitor)
        self.assertEqual(event_time_tracking.session, self.session)

    def test_bad_creating_event_time_tracking(self):
        data = {"event_type": LandingEvent.Types.TIME_TRACKING}

        self._test_event_without_visitor(data)
        self._test_event_without_session(data)
        self._test_event_wit_wrong_visitor(data)
        self._test_event_wit_wrong_session(data)

    def test_bad_creating_event_transition_to(self):
        data = {
            "event_type": LandingEvent.Types.TRANSITION_TO,
            "product_source": 10,
        }
        headers = {
            "HTTP_X-SESSION": self.session,
            "HTTP_COOKIE": "visitor={}".format(self.visitor),
        }

        self._test_event_without_visitor(data)
        self._test_event_without_session(data)
        self._test_event_wit_wrong_visitor(data)
        self._test_event_wit_wrong_session(data)

        data = {"event_type": LandingEvent.Types.TRANSITION_TO}
        response = self.client.post(self.url, data, **headers)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data["error_code"].code, PRODUCT_SOURCE_ID_NOT_SET)

        data = {
            "event_type": LandingEvent.Types.TRANSITION_TO,
            "product_source": 1000,
        }
        response = self.client.post(self.url, data, **headers)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.data,
            {
                "product_source": [
                    ErrorDetail(
                        string='Invalid pk "1000" - object does not exist.',
                        code="does_not_exist",
                    )
                ]
            },
        )
