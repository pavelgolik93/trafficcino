from datetime import timedelta, datetime

from django.db.models import (
    F,
    Count,
    Q,
    CharField,
    Value,
    IntegerField,
    When,
    Case,
    Subquery,
    OuterRef,
    Avg,
)
from django.db.models.fields.json import KeyTextTransform
from django.db.models.functions import Coalesce, Cast, Extract
from django.utils import timezone

from analytics.models import LandingEvent
from analytics.utils import (
    atc_rate_calculation,
    slicing_date_range_to_timestamp_sub_range,
)


class Base:
    def __init__(
        self,
        user_id: int,
        landing_pool_id: int | None = None,
        date_range: list | tuple[datetime, datetime] | None = None,
    ):
        self.user_id = user_id
        self.landing_pool_id = landing_pool_id
        self.date_range = self._set_date_range(date_range)

    @property
    def _filer_by_user_and_landing_pool_id(self) -> dict:
        params = {"landing__landing_pool__user_id": self.user_id}
        if self.landing_pool_id is not None:
            params.update({"landing__landing_pool_id": self.landing_pool_id})
        return params

    @property
    def _filter_by_date_range(self) -> dict:
        if self.date_range is None:
            return {}

        start_date, end_date = self.date_range
        if start_date == end_date:
            return {"created_at__date": start_date}
        return {"created_at__date__gte": start_date, "created_at__date__lte": end_date}

    @property
    def _filter_params(self) -> dict:
        return {**self._filer_by_user_and_landing_pool_id, **self._filter_by_date_range}

    def _set_date_range(self, date_range: list[str, str] | None):
        if date_range is None:
            date_now = timezone.now()
            return date_now - timedelta(days=30), date_now

        date_start, date_end = date_range
        return self._str_to_datetime(date_start), self._str_to_datetime(date_end)

    def _str_to_datetime(self, date: str | datetime) -> datetime:
        return (
            date if isinstance(date, datetime) else datetime.strptime(date, "%Y-%m-%d")
        )


class Metrics(Base):
    ranges_count = 20

    def _case_by_timestamp_chunks(self, ranges_count: int) -> Case:
        cases = [
            When(timestamp__gte=s, timestamp__lt=e, then=Value(i))
            for i, (s, e) in enumerate(
                slicing_date_range_to_timestamp_sub_range(
                    *self.date_range, ranges_count=ranges_count
                )
            )
        ]
        return Case(*cases)

    def _set_empty_groups(self, data_list: list[dict]) -> list[dict]:
        new_data = [{"group": i, "value": 0} for i in range(self.ranges_count)]
        for obj in data_list:
            group_index = obj["group"]
            if group_index is None:
                continue
            new_data[group_index] = obj
        return new_data

    def _sum_values_current_and_past(self, data_list: list[dict]) -> tuple[int, int]:
        value_by_date_range = 0
        value_past_ranges = 0
        for obj in data_list:
            value = obj["value"]
            if obj["group"] is None:
                value_past_ranges += value
                continue
            value_by_date_range += value
        return value_by_date_range, value_past_ranges

    def _calculate_dynamics(
        self, value: int | float, before_value: int | float
    ) -> float | None:
        return round((value / before_value) * 100, 2) if before_value != 0 else None

    def _calculate_atc_rate(self, add_to_carts: int, views: int) -> float:
        return round((add_to_carts / views) * 100, 2) if views != 0 else 0

    def _calculating(self, data: list[dict]) -> tuple:
        value_by_date_range, value_past_ranges = self._sum_values_current_and_past(data)
        dynamics = self._calculate_dynamics(value_by_date_range, value_past_ranges)
        graph = self._set_empty_groups(data)
        return graph, value_by_date_range, dynamics, value_past_ranges

    def _page_views(self, ranges_count=20):
        data = list(
            LandingEvent.objects.filter(
                **self._filer_by_user_and_landing_pool_id,
                created_at__date__lte=self.date_range[1],
                event_type=LandingEvent.Types.NEW_VISIT,
            )
            .annotate(
                timestamp=Cast(Extract("created_at", "epoch"), IntegerField()),
                group=self._case_by_timestamp_chunks(ranges_count=ranges_count),
            )
            .values("group")
            .annotate(value=Count("id"))
            .order_by("group")
        )

        graph, value, dynamics, past_page_views = self._calculating(data)
        return {"graph": graph, "value": value, "dynamics": dynamics}, past_page_views

    def _average_time_spent(self):
        data = list(
            LandingEvent.objects.filter(
                **self._filer_by_user_and_landing_pool_id,
                created_at__date__lte=self.date_range[1],
                event_type=LandingEvent.Types.NEW_VISIT,
            )
            .annotate(
                count_time_tracking_events=Coalesce(
                    Subquery(
                        LandingEvent.objects.filter(
                            session=OuterRef("session"),
                            event_type=LandingEvent.Types.TIME_TRACKING,
                        )
                        .values("session")
                        .annotate(count=Count("session"))
                        .values("count")
                    ),
                    Value(0),
                ),
                seconds=F("count_time_tracking_events") * 5,
                timestamp=Cast(Extract("created_at", "epoch"), IntegerField()),
                group=self._case_by_timestamp_chunks(ranges_count=self.ranges_count),
            )
            .values("group")
            .annotate(value=Avg("seconds"))
        )

        graph, value, dynamics, _ = self._calculating(data)
        return {
            "graph": graph,
            "value": round(value, 2),
            "dynamics": dynamics if dynamics is None else round(dynamics, 2),
        }

    def _add_to_carts(self):
        data = list(
            LandingEvent.objects.filter(
                **self._filer_by_user_and_landing_pool_id,
                created_at__date__lte=self.date_range[1],
                event_type=LandingEvent.Types.TRANSITION_TO,
            )
            .annotate(
                timestamp=Cast(Extract("created_at", "epoch"), IntegerField()),
                group=self._case_by_timestamp_chunks(ranges_count=self.ranges_count),
            )
            .values("group")
            .annotate(value=Count("id"))
            .order_by("group")
        )

        graph, value, dynamics, past_add_to_carts = self._calculating(data)
        return {"graph": graph, "value": value, "dynamics": dynamics}, past_add_to_carts

    def _add_to_cart_rate(
        self, page_views_data, past_page_views, add_to_carts_data, past_add_to_carts
    ):
        add_to_cart_rate_by_date_range = self._calculate_atc_rate(
            add_to_carts_data["value"], page_views_data["value"]
        )

        graph = []
        for atc_data, pv_data in zip(
            add_to_carts_data["graph"], page_views_data["graph"]
        ):
            assert atc_data["group"] == pv_data["group"]
            graph.append(
                {
                    "group": atc_data["group"],
                    "value": self._calculate_atc_rate(
                        atc_data["value"], pv_data["value"]
                    ),
                }
            )

        past_add_to_cart_rate = self._calculate_atc_rate(
            past_add_to_carts, past_page_views
        )
        dynamics = self._calculate_dynamics(
            add_to_cart_rate_by_date_range, past_add_to_cart_rate
        )
        return {
            "graph": graph,
            "value": add_to_cart_rate_by_date_range,
            "dynamics": dynamics,
        }

    def get(self):
        page_views_data, past_page_views = self._page_views()
        add_to_carts_data, past_add_to_carts = self._add_to_carts()
        return {
            "page_views": page_views_data,
            "average_time_spent": self._average_time_spent(),
            "add_to_carts": add_to_carts_data,
            "add_to_cart_rate": self._add_to_cart_rate(
                page_views_data, past_page_views, add_to_carts_data, past_add_to_carts
            ),
        }


class Insights(Base):
    def _metrics(self):
        return Metrics(self.user_id, self.landing_pool_id, self.date_range).get()

    def _top_commerce_experiences(self):
        commerce_experiences = list(
            LandingEvent.objects.filter(
                **self._filter_params,
                event_type__in=(
                    LandingEvent.Types.NEW_VISIT,
                    LandingEvent.Types.TRANSITION_TO,
                ),
            )
            .values("landing__landing_pool")
            .annotate(
                landing_pool_name=F("landing__landing_pool__name"),
                page_views=Count(
                    "id", filter=Q(event_type=LandingEvent.Types.NEW_VISIT)
                ),
                add_to_carts=Count(
                    "id", filter=Q(event_type=LandingEvent.Types.TRANSITION_TO)
                ),
            )
            .values("landing_pool_name", "page_views", "add_to_carts")
            .order_by("-page_views")
        )
        return atc_rate_calculation(commerce_experiences)

    def _top_platforms(self):
        platforms = list(
            LandingEvent.objects.filter(
                **self._filter_params,
                event_type__in=(
                    LandingEvent.Types.NEW_VISIT,
                    LandingEvent.Types.TRANSITION_TO,
                ),
            )
            .values("landing__ad_network_id")
            .annotate(
                ad_network_id=F("landing__ad_network_id"),
                page_views=Count(
                    "id", filter=Q(event_type=LandingEvent.Types.NEW_VISIT)
                ),
                add_to_carts=Count(
                    "id", filter=Q(event_type=LandingEvent.Types.TRANSITION_TO)
                ),
            )
            .values("ad_network_id", "page_views", "add_to_carts")
            .order_by("-page_views")
        )
        return atc_rate_calculation(platforms)

    def _top_retailers(self):
        retailers = list(
            LandingEvent.objects.filter(
                **self._filter_params,
                event_type__in=(
                    LandingEvent.Types.NEW_VISIT,
                    LandingEvent.Types.TRANSITION_TO,
                ),
            )
            .values("landing__landing_pool__products__sources__marketplace_id")
            .annotate(
                marketplace_id=F(
                    "landing__landing_pool__products__sources__marketplace_id"
                ),
                page_views=Count(
                    "id",
                    filter=Q(event_type=LandingEvent.Types.NEW_VISIT),
                    distinct=True,
                ),
                add_to_carts=Count(
                    "id",
                    filter=Q(
                        event_type=LandingEvent.Types.TRANSITION_TO,
                        product_source__marketplace_id=F("marketplace_id"),
                    ),
                    distinct=True,
                ),
            )
            .values("marketplace_id", "page_views", "add_to_carts")
            .order_by("-page_views")
        )
        return atc_rate_calculation(retailers)

    def _region_performance(self):
        return list(
            LandingEvent.objects.filter(
                **self._filter_params,
                event_type=LandingEvent.Types.NEW_VISIT,
            )
            .values("ip_address__country_name")
            .annotate(
                country_name=Coalesce(
                    KeyTextTransform(
                        "country_name", "ip_address", output_field=CharField()
                    ),
                    Value("Other"),
                ),
                page_views=Count("id"),
            )
            .values("country_name", "page_views")
            .order_by("-page_views")
        )

    def _devices(self):
        views_by_devices = LandingEvent.objects.filter(
            **self._filter_params,
            event_type=LandingEvent.Types.NEW_VISIT,
        ).aggregate(
            pc_page_views=Count("event_type", filter=Q(user_agent__is_pc=True)),
            mobile_page_views=Count("event_type", filter=Q(user_agent__is_mobile=True)),
            tablet_page_views=Count("event_type", filter=Q(user_agent__is_tablet=True)),
            other_page_views=Count(
                "event_type",
                filter=Q(
                    user_agent__is_pc=False,
                    user_agent__is_mobile=False,
                    user_agent__is_tablet=False,
                ),
            ),
        )
        return [
            {"device": "PC", "page_views": views_by_devices["pc_page_views"]},
            {"device": "Mobile", "page_views": views_by_devices["mobile_page_views"]},
            {"device": "Tablet", "page_views": views_by_devices["tablet_page_views"]},
            {"device": "Other", "page_views": views_by_devices["other_page_views"]},
        ]

    def _operating_systems(self):
        return list(
            LandingEvent.objects.filter(
                **self._filter_params,
                event_type=LandingEvent.Types.NEW_VISIT,
            )
            .values("user_agent__os__family")
            .annotate(os_name=F("user_agent__os__family"), page_views=Count("id"))
            .values("os_name", "page_views")
            .order_by("-page_views")
        )

    def _browsers(self):
        return list(
            LandingEvent.objects.filter(
                **self._filter_params,
                event_type=LandingEvent.Types.NEW_VISIT,
            )
            .values("user_agent__user_agent__family")
            .annotate(
                browser_name=F("user_agent__user_agent__family"), page_views=Count("id")
            )
            .values("browser_name", "page_views")
            .order_by("-page_views")
        )

    def overview_analytics(self):
        return {
            "top_commerce_experiences": self._top_commerce_experiences(),
            "metrics": self._metrics(),
        }

    def get(self):
        return {
            "metrics": self._metrics(),
            "top_platforms": self._top_platforms(),
            "top_retailers": self._top_retailers(),
            "region_performance": self._region_performance(),
            "devices": self._devices(),
            "operating_systems": self._operating_systems(),
            "browsers": self._browsers(),
        }
